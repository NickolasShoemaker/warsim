#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>


/*
* A simple c++ style logger that uses iostreams to output to terminal
* and optionally a file.
*
* Dont bother with levels, only important info should be logged anyway
*/
class logger{
	std::ofstream* log_stream;
	std::ostream* term_stream;
	std::stringstream msg;
public:
	//constexpr int logger_append;	//If file exists append to it
	//constexpr int logger_clear;		//If file exists clear it and start over

	logger(){
		term_stream = &std::cerr;
		log_stream = nullptr;
	}
	logger(const char* log_file){
		term_stream = &std::cerr;
		log_stream = new std::ofstream(log_file, std::ofstream::app);
	}
	~logger(){
		if(log_stream){
			log_stream->close();
			delete log_stream;
		}

	}

	/*
	* This lets up default construct and add a file later
	* This is useful to a class member log
	*/
	void set_file(const char* log_file){
		if(log_stream){
			log_stream->close();
			delete log_stream;
		}
		log_stream = new std::ofstream(log_file, std::ofstream::app);
	}

	//-----------------------------------------------
	// Logger output streams
	//-----------------------------------------------
	/*
	* Stream out type T
	*/
	template<typename T>
	logger& operator<<(T value){
		msg << value;
		return *this;
	}

	/*
	* Stream out thread ID
	*/
	logger& operator<<(std::thread::id value){
		msg << value;
		return *this;
	}

	/*
	* Pass on stream manipulators like std::endl
	*/
	logger& operator<<(std::ostream&(*pManip)(std::ostream&)){
		(*pManip)(msg);
		return *this;
	}

	/*
	* Get logger reference and print message stub
	*/
	logger& operator()(){
		// date time
		header();
		return *this;
	}

	logger& operator()(const char* file_macro, int line_number){
		*this << file_macro << ":" << line_number << "  ";
		header();
		return *this;
	}

	void header(){
		*this << "Thread " << std::this_thread::get_id() << ":  ";
	}

	/*
	* Send the composed log message out
	*/
	void log(){
		*term_stream << msg.str();
		if(log_stream)
			*log_stream << msg.str();
		msg.str("");
	}
};

#endif //LOGGER_H