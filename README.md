# Warsim
## Nick Shoemaker

## Pitch
Warsim is the working title of a 3D roleplaying strategy managment game. The game takes place in a voxel world of uint16 coordinate space makeing each axis 65535 voxels long including the y axis. The world loops back on itself in the x and z directions giving the illusion of a spherical world.

There are various civilizations in this world with people living out their lives. The player can take the place of any of these and roleplay as anyone from a common farmer, merchant, hero, or even a king. The player can set off into the winderness to find their fortune or set to building a kingdom. The sky is the limit and since it is 65535 voxels high that is a large limit.

## Dependencies
OpenGL 3.0+
GLFW
c++11

## Notes
The polyVox library is just for testing purposes and will be removed when I am done designing my own mesh system.