#include "perlin_noise.h"

// Hash lookup table as defined by Ken Perlin.
// This is a randomly arranged array of all numbers from 0-255 inclusive.
constexpr int perlin_noise::permutation_table[512];