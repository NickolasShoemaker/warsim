#ifndef PERLIN_NOISE_H
#define PERLIN_NOISE_H

#include <stdlib.h>
#include <limits>

/*
* A class for generating 3D Perlin noise.
*/
class perlin_noise{
	// Hash lookup table as defined by Ken Perlin.
	// This is a randomly arranged array of all numbers from 0-255 inclusive.
	static constexpr int permutation_table[512] = {
		151, 160, 137,  91,  90,  15, 131,  13, 201,  95,  96,  53, 194, 233,   7, 225,
		140,  36, 103,  30,  69, 142,   8,  99,  37, 240,  21,  10,  23, 190,   6, 148,
		247, 120, 234,  75,   0,  26, 197,  62,  94, 252, 219, 203, 117,  35,  11,  32,
		 57, 177,  33,  88, 237, 149,  56,  87, 174,  20, 125, 136, 171, 168,  68, 175,
		 74, 165,  71, 134, 139,  48,  27, 166,	 77, 146, 158, 231,  83, 111, 229, 122,
		 60, 211, 133, 230, 220, 105,  92,  41,  55,  46, 245,  40, 244, 102, 143,  54,
		 65,  25,  63, 161,   1, 216,  80,  73, 209,  76, 132, 187, 208,  89,  18, 169,
		200, 196, 135, 130, 116, 188, 159,  86, 164, 100, 109, 198, 173, 186,   3,  64,
		 52, 217, 226, 250, 124, 123,   5, 202,  38, 147, 118, 126, 255,  82,  85, 212,
		207, 206,  59, 227,  47,  16,  58,  17, 182, 189,  28,  42,	223, 183, 170, 213,
		119, 248, 152,   2,  44, 154, 163,  70, 221, 153, 101, 155, 167,  43, 172,   9,
		129,  22,  39, 253,  19,  98, 108, 110,  79, 113, 224, 232, 178, 185, 112, 104,
		218, 246,  97, 228,	251,  34, 242, 193, 238, 210, 144,  12, 191, 179, 162, 241,
		 81,  51, 145, 235, 249,  14, 239, 107,	 49, 192, 214,  31, 181, 199, 106, 157,
		 84,  84, 204, 176, 115, 121,  50,  45, 127,   4, 150, 254,	138, 236, 205,  93,
		222, 114,  67,  29,  24,  72, 243, 141, 128, 195,  78,  66, 215,  61, 156, 180,

		151, 160, 137,  91,  90,  15, 131,  13, 201,  95,  96,  53, 194, 233,   7, 225,
		140,  36, 103,  30,  69, 142,   8,  99,  37, 240,  21,  10,  23, 190,   6, 148,
		247, 120, 234,  75,   0,  26, 197,  62,  94, 252, 219, 203, 117,  35,  11,  32,
		 57, 177,  33,  88, 237, 149,  56,  87, 174,  20, 125, 136, 171, 168,  68, 175,
		 74, 165,  71, 134, 139,  48,  27, 166,	 77, 146, 158, 231,  83, 111, 229, 122,
		 60, 211, 133, 230, 220, 105,  92,  41,  55,  46, 245,  40, 244, 102, 143,  54,
		 65,  25,  63, 161,   1, 216,  80,  73, 209,  76, 132, 187, 208,  89,  18, 169,
		200, 196, 135, 130, 116, 188, 159,  86, 164, 100, 109, 198, 173, 186,   3,  64,
		 52, 217, 226, 250, 124, 123,   5, 202,  38, 147, 118, 126, 255,  82,  85, 212,
		207, 206,  59, 227,  47,  16,  58,  17, 182, 189,  28,  42,	223, 183, 170, 213,
		119, 248, 152,   2,  44, 154, 163,  70, 221, 153, 101, 155, 167,  43, 172,   9,
		129,  22,  39, 253,  19,  98, 108, 110,  79, 113, 224, 232, 178, 185, 112, 104,
		218, 246,  97, 228,	251,  34, 242, 193, 238, 210, 144,  12, 191, 179, 162, 241,
		 81,  51, 145, 235, 249,  14, 239, 107,	 49, 192, 214,  31, 181, 199, 106, 157,
		 84,  84, 204, 176, 115, 121,  50,  45, 127,   4, 150, 254,	138, 236, 205,  93,
		222, 114,  67,  29,  24,  72, 243, 141, 128, 195,  78,  66, 215,  61, 156, 180,
	};

	/*
	* An ease curve function to make the approach to asymptote more gradual after interpolation
	*/
	static double fade(double t){
		//return t * t * t * (t * (t * 6 - 15) + 10);         // 6t^5 - 15t^4 + 10t^3
		return t * t * (3.0f - 2.0f * t);
	}

	/*
	* The gradient function calculates the dot product between a pseudorandom
	* gradient vector and the vector from the input coordinate to the 8 unit
	* cube cornors
	*/
	static double grad(int hash, double x, double y, double z){
		switch(hash & 0xF){
		    case 0x0: return  x + y;
		    case 0x1: return -x + y;
		    case 0x2: return  x - y;
		    case 0x3: return -x - y;
		    case 0x4: return  x + z;
		    case 0x5: return -x + z;
		    case 0x6: return  x - z;
		    case 0x7: return -x - z;
		    case 0x8: return  y + z;
		    case 0x9: return -y + z;
		    case 0xA: return  y - z;
		    case 0xB: return -y - z;
		    case 0xC: return  y + x;
		    case 0xD: return -y + z;
		    case 0xE: return  y - x;
		    case 0xF: return -y - z;
		    default: return 0; // never happens
		}
	}

public:
	/*
	* Interpolate n between starting_point and endding_point
	*/
	static double linear_interpolation(double n, double starting_point, double endding_point){
		return starting_point + n * (endding_point - starting_point);
	}

	/*
	* Get a value in a different range
	*/
	static double convert_range(double value, double OldMin, double OldMax, double NewMin, double NewMax){
		return (((value - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
	}

	/*
	* Get a noice value [0,1] for a 3D point using Perlin Noise Improved
	* Using octaves to increase granularity by nesting calls.
	* Warning: more octaves have diminishing returns and greater costs
	*/
	static double get(double x, double y, double z, int octaves, double persistence, double frequency = 1, double amplitude = 1) {
	    double total = 0;
	    double maxValue = 0;  // Used for normalizing result to 0.0 - 1.0

	    for(int i = 0; i < octaves; i++) {
	        total += get(x * frequency, y * frequency, z * frequency) * amplitude;
	        
	        maxValue += amplitude;
	        
	        amplitude *= persistence;
	        frequency *= 2;
	    }
	    
	    return total / maxValue;
	}

	/*
	* Get a noice value [0,1] for a 3D point using Perlin Noise Improved
	*/
	static double get(double x, double y, double z){

		/*
		* Get the unit cube this coord is in. It repeats after 255
		*/
		int unit_x = (int)x & 255;
		int unit_y = (int)y & 255;
		int unit_z = (int)z & 255;

		/*
		* We correct the point to be in unit cube space
		* by making it between 0.0f and 1.0f		
		*/
		double unit_x_point_location = x - (int)x;
		double unit_y_point_location = y - (int)y;
		double unit_z_point_location = z - (int)z;

		double unit_x_point_location_smoothed = fade(x);
		double unit_y_point_location_smoothed = fade(y);
		double unit_z_point_location_smoothed = fade(z);

		/*
		* Get the 8 cornors of the unit cube around our point
		*/
		int unit_cube_cornor_x0y0z0 = permutation_table[ permutation_table[ permutation_table[ unit_x    ] + unit_y    ] + unit_z    ];
		int unit_cube_cornor_x0y1z0 = permutation_table[ permutation_table[ permutation_table[ unit_x    ] + unit_y + 1] + unit_z    ];
		int unit_cube_cornor_x0y0z1 = permutation_table[ permutation_table[ permutation_table[ unit_x    ] + unit_y    ] + unit_z + 1];
		int unit_cube_cornor_x0y1z1 = permutation_table[ permutation_table[ permutation_table[ unit_x    ] + unit_y + 1] + unit_z + 1];
		int unit_cube_cornor_x1y0z0 = permutation_table[ permutation_table[ permutation_table[ unit_x + 1] + unit_y    ] + unit_z    ];
		int unit_cube_cornor_x1y1z0 = permutation_table[ permutation_table[ permutation_table[ unit_x + 1] + unit_y + 1] + unit_z    ];
		int unit_cube_cornor_x1y0z1 = permutation_table[ permutation_table[ permutation_table[ unit_x + 1] + unit_y    ] + unit_z + 1];
		int unit_cube_cornor_x1y1z1 = permutation_table[ permutation_table[ permutation_table[ unit_x + 1] + unit_y + 1] + unit_z + 1];

		/*
		* Linearly interpolate the gradient vector from the unit cube cornors and our unit point
		* First interpolate lines on x axis for the z = 0 face for our point x location
		* Then then interpolate the result for our point y location
		* repeat for the z = 1 face
		* Then then interpolate the result for our point y location again
		* Then finally interpolate in the z direction and we have our value
		*/
		double unit_line_y0z0 = linear_interpolation(	grad(unit_cube_cornor_x0y0z0, unit_x_point_location  , unit_y_point_location  , unit_z_point_location),
														grad(unit_cube_cornor_x1y0z0, unit_x_point_location-1, unit_y_point_location  , unit_z_point_location),	unit_x_point_location_smoothed);						
		double unit_line_y1z0 = linear_interpolation(	grad(unit_cube_cornor_x0y1z0, unit_x_point_location  , unit_y_point_location-1, unit_z_point_location),
														grad(unit_cube_cornor_x1y1z0, unit_x_point_location-1, unit_y_point_location-1, unit_z_point_location), unit_x_point_location_smoothed);

		double unit_face_z0 = linear_interpolation(unit_line_y0z0, unit_line_y1z0, unit_y_point_location_smoothed);

		double unit_line_y0z1 = linear_interpolation(	grad(unit_cube_cornor_x0y0z1, unit_x_point_location  , unit_y_point_location  , unit_z_point_location-1),
														grad(unit_cube_cornor_x1y0z1, unit_x_point_location-1, unit_y_point_location  , unit_z_point_location-1), unit_x_point_location_smoothed);
		double unit_line_y1z1 = linear_interpolation(	grad(unit_cube_cornor_x0y1z1, unit_x_point_location  , unit_y_point_location-1, unit_z_point_location-1),
		          										grad(unit_cube_cornor_x1y1z1, unit_x_point_location-1, unit_y_point_location-1, unit_z_point_location-1), unit_x_point_location_smoothed);

		double unit_face_z1 = linear_interpolation(unit_line_y0z1, unit_line_y1z1, unit_y_point_location_smoothed);

		double ret = linear_interpolation(unit_face_z0, unit_face_z1, unit_z_point_location_smoothed);
		for(int i = 0; i < std::numeric_limits<double>::max_digits10; i++){
			ret /= 10;
		}
		if(ret > 2)
			ret = 2;
		else if(ret < -2)
			ret = -2;
		return convert_range(ret, 2, -2, 1, 0);
	}
};

#endif //PERLIN_NOISE_Hendif