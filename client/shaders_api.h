#ifndef SHADER_API_H
#define SHADER_API_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

GLuint load_shader(const char* const*, GLenum);
GLuint load_shader_file(const char*, GLenum);

void print_program_log(GLuint);
void print_shader_log(GLuint);

#endif //SHADER_API_H