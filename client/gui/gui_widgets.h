#ifndef GUI_WIDGETS_H
#define GUI_WIDGETS_H

#include <cstdio>

#include "nk_wrapper.h"
#include "client.h"

/*
* Nuklear widgets
*
*/
namespace gui_widgets{
	/*
	* A dropdown menu
	*/
	void game_menu(client* Client);
};

#endif //GUI_WIDGETS_H