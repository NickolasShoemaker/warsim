#include "gui_widgets.h"

namespace gui_widgets{
	void game_menu(client* Client){
	    if(nk_begin(Client->ctx, "Game Menu\0", nk_rect(0, 0, 250, 60), NK_WINDOW_BACKGROUND|NK_WINDOW_NO_SCROLLBAR)){
	        nk_menubar_begin(Client->ctx);
	            nk_layout_row_dynamic(Client->ctx, 20, 2);
	      
	            if(nk_menu_begin_text(Client->ctx, "Main menu\0", 9, NK_TEXT_ALIGN_LEFT, nk_vec2(160,300))){
	                nk_layout_row_dynamic(Client->ctx, 20, 1);
	                if(nk_menu_item_text(Client->ctx, "Noise Screen\0", 12, NK_TEXT_ALIGN_LEFT))
	                    Client->set_current_scene("Noise Screen");
	                if(nk_menu_item_text(Client->ctx, "Voxel Screen\0", 12, NK_TEXT_ALIGN_LEFT))
	                    Client->set_current_scene("Voxel Screen");
	                if(nk_menu_item_text(Client->ctx, "Game Screen\0", 11, NK_TEXT_ALIGN_LEFT))
	                    Client->set_current_scene("Game Screen");
	                if(nk_menu_item_text(Client->ctx, "menu\0", 4, NK_TEXT_ALIGN_LEFT))
	                    printf("this happened I guess\n");
	                nk_menu_end(Client->ctx);
	            }
	      
	            if(nk_menu_begin_text(Client->ctx, "Main menu2\0", 10, NK_TEXT_ALIGN_LEFT, nk_vec2(160,300))){
	                nk_layout_row_dynamic(Client->ctx, 20, 1);
	                if(nk_menu_item_text(Client->ctx, "Voxel Screen\0", 12, NK_TEXT_ALIGN_LEFT))
	                    Client->set_current_scene("Voxel Screen");
	                if(nk_menu_item_text(Client->ctx, "menu\0", 4, NK_TEXT_ALIGN_LEFT))
	                    printf("this happened I guess\n");
	                if(nk_menu_item_text(Client->ctx, "menu\0", 4, NK_TEXT_ALIGN_LEFT))
	                    printf("this happened I guess\n");
	                if(nk_menu_item_text(Client->ctx, "menu\0", 4, NK_TEXT_ALIGN_LEFT))
	                    printf("this happened I guess\n");
	                nk_menu_end(Client->ctx);
	            }
	        nk_menubar_end(Client->ctx);
	    }
	    nk_end(Client->ctx);
	}
};