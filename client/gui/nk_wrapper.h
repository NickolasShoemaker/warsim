/******************************************************************************
* We use this to define the values for and to include nuklear so can have it  *
* as an object file and speed up compilation a lot.                           *
* Nick Shoemaker                                                              *
*                                                                             *
******************************************************************************/
#ifndef NK_WRAPPER_H
#define NK_WRAPPER_H

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT

#include "nuklear.h"

#endif //NK_WRAPPER_H