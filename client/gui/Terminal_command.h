#ifndef TERMINAL_COMMAND_H
#define TERMINAL_COMMAND_H

#include <string>

namespace gui_widgets{
		
	class Terminal;

	/*
	* Abstract class to overload for a more complex command
	*
	* Each command has a prototype
	* Need input and output to Terminal, through term pointer
	*/
	class Terminal_command{
	public:
		virtual std::string get_command() = 0;
		virtual int get_command_length() = 0;

		// Run the command
		virtual void run(Terminal* term, std::string args) = 0;

		// A short explanation for this command when help is used
		virtual std::string help() = 0;

		// A specific explanation for when help is called on this command
		// A call prototype with arguments if any
		virtual std::string help_message() = 0;
	};

	/*
	* If the command is simple an instance of this class
	* can be used instead
	*
	* lambda to run
	*/
	class basic_command: public Terminal_command{
		std::string m_command;
		void (*run_command)(Terminal*, std::string);
		std::string m_help;
		std::string m_help_message;
	public:
		basic_command(std::string com, std::string a_help, std::string a_help_message, void (*command_ptr)(Terminal*, std::string)){
			m_command = com;
			run_command = command_ptr;
			m_help = a_help;
			m_help_message = a_help_message;
		}

		std::string get_command(){
			return m_command;
		}

		int get_command_length(){
			return m_command.size();
		}

		void run(Terminal* term, std::string args){
			run_command(term, args);
		}

		std::string help(){
			return m_help;
		}

		std::string help_message(){
			return m_help_message;
		}
	};
};

#endif //TERMINAL_COMMAND_H