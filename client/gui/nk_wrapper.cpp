/******************************************************************************
* We use this to define the values for and to include nuklear so can have it  *
* as an object file and speed up compilation a lot.                           *
* Nick Shoemaker                                                              *
*                                                                             *
******************************************************************************/
#include "nk_wrapper.h"

#define NK_IMPLEMENTATION
#include "nuklear.h"