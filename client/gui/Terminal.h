#ifndef TERMINAL_H
#define TERMINAL_H

#include <cstring>
#include <unordered_map>
#include <stack>
#include <memory>
#include <vector>

#include "Terminal_command.h"

namespace gui_widgets{

	/*
	* An interactive command prompt
	*/
	class Terminal{
		int editerLen = 0;
		std::string editerBuffer;
		unsigned int commandStart = 0;
		
		// we can handle multiple instances if we add an pid
		// but we will need a window manager
		static short instance;

		struct nk_context *ctx;
		struct GLFWwindow *win;

		bool control_shift;
		bool control_ctr;

		std::string prompt = "Term@War Sim>";
		const char* Title = "Terminal";

		std::vector<std::shared_ptr<Terminal_command>> command_registry;
	public:
		Terminal(struct GLFWwindow *window, struct nk_context *context){
			ctx = context;
			win = window;

			control_shift = false;
			control_ctr = false;

			editerBuffer += prompt;
			commandStart = editerBuffer.size();

			command_registry.push_back(std::shared_ptr<Terminal_command>(new basic_command("clear", "clears the terminal buffer", "clear\n clears terminal buffer", [](Terminal* term, std::string arg){term->Terminal_clear();})));
			command_registry.push_back(std::shared_ptr<Terminal_command>(new basic_command("hide", "hides the terminal window", "hide\n hides the terminal window", [](Terminal* term, std::string arg){nk_window_show(term->get_nk_context(), term->get_title(), NK_HIDDEN);term->write_string("\n");})));
			command_registry.push_back(std::shared_ptr<Terminal_command>(new basic_command("say", "echo a string back in the terminal", "say [string]", [](Terminal* term, std::string arg){term->write_string("\n");term->write_string(arg);term->write_string("\n");})));
			command_registry.push_back(std::shared_ptr<Terminal_command>(new basic_command("help", "Runs this command", "help [command]\n   Provides information about the command.", [](Terminal* term, std::string arg){std::vector<std::shared_ptr<Terminal_command>> command_registry = term->get_registry();
														if(arg == ""){
															for(auto it = command_registry.begin(); it != command_registry.end(); it++){
																term->write_string("\n");
																term->write_string((*it)->get_command());
																term->write_string(":       ");
																term->write_string((*it)->help());
															}
															term->write_string("\n");
														}else{
															for(auto it = command_registry.begin(); it != command_registry.end(); it++){
																if((*it)->get_command() == arg){
																	term->write_string("\n");
																	term->write_string((*it)->help_message());
																	term->write_string("\n");															}
																}
														}})));
		}

		void register_command(std::shared_ptr<Terminal_command> com){
			command_registry.push_back(com);
		}

		void remove_command(std::string com){
			for(auto it = command_registry.begin(); it != command_registry.end(); it++){
				if((*it)->get_command() == com)
					command_registry.erase(it);
			}
		}
		
		std::vector<std::shared_ptr<Terminal_command>>& get_registry(){
			return command_registry;
		}

		std::string get_prompt(){
			return prompt;
		}

		void set_prompt(std::string prompt){
			this->prompt = prompt;
		}

		/*
		* Add all the draw command for Terminal to be renderer
		*/
		void render(){
			nk_style_push_style_item(ctx, &ctx->style.window.fixed_background, nk_style_item_hide());
    		nk_style_push_vec2(ctx, &ctx->style.window.min_size, nk_vec2(130, 200));
			nk_style_push_vec2(ctx, &ctx->style.window.spacing, nk_vec2(0, 0));
			nk_style_push_vec2(ctx, &ctx->style.window.padding, nk_vec2(0, 0));

			nk_style_push_style_item(ctx, &ctx->style.edit.normal, nk_style_item_hide());
 	      	nk_style_push_style_item(ctx, &ctx->style.edit.hover, nk_style_item_hide());
    		nk_style_push_style_item(ctx, &ctx->style.edit.active, nk_style_item_hide());
    		nk_style_push_vec2(ctx, &ctx->style.edit.padding, nk_vec2(4, 20));
    		//nk_style_push_float(ctx, &ctx->style.edit.border, 0);

			if(nk_begin(ctx, Title, nk_rect(300, 50, 301, 250), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_NO_SCROLLBAR|NK_WINDOW_SCALABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_CLOSABLE|NK_WINDOW_TITLE)){
				nk_layout_row_dynamic(ctx, nk_window_get_height(ctx) - 30, 1);
				editerLen = editerBuffer.size();
				nk_edit_string(ctx, NK_EDIT_MULTILINE | NK_EDIT_GOTO_END_ON_ACTIVATE, &editerBuffer[0], &editerLen, editerBuffer.capacity(), nk_filter_ascii);
			}
			nk_end(ctx);

			//nk_style_pop_float(ctx);
			nk_style_pop_vec2(ctx);
			nk_style_pop_style_item(ctx);
			nk_style_pop_style_item(ctx);
			nk_style_pop_style_item(ctx);

			nk_style_pop_vec2(ctx);
			nk_style_pop_vec2(ctx);
			nk_style_pop_vec2(ctx);
			nk_style_pop_style_item(ctx);
		}

		const char* get_title(){
			return Title;
		}
		
		struct nk_context* get_nk_context(){
			return ctx;
		}

		/*
		* Only add chars after commandStart
		*/
		void add_char(char value){

			struct nk_window* term = nk_window_find(ctx, Title);
			if(term){
				if(term->edit.cursor < commandStart)
					return;

				// adding to the end
				if(term->edit.cursor == editerBuffer.size()){
					editerBuffer += value;
				
				// inserting
				}else{
					//ripple or insert
					 char insert = value;
					 for(int i = ctx->text_edit.cursor; i <= editerBuffer.size(); i++){
					 	char temp = editerBuffer[i];
					 	editerBuffer[i] = insert;
					 	insert = temp;
					 }
				}

				term->edit.cursor++;
			}
		}

		/*
		* Only remove char from commandStart on
		*/
		void remove_char(bool right){
			struct nk_window* term = nk_window_find(ctx, Title);
			if(term){
				if(ctx->text_edit.cursor != editerBuffer.size() && right){
					for(int i = ctx->text_edit.cursor + 1; i < editerBuffer.size(); i++){
					 	editerBuffer[i - 1] = editerBuffer[i];
					 }
					 return;
				}else if(editerBuffer.size() <= commandStart){
					return;
				}else{
					if(ctx->text_edit.cursor == editerBuffer.size()){
						editerBuffer.pop_back();
					}else{
						 for(int i = ctx->text_edit.cursor; i < editerBuffer.size(); i++){
						 	editerBuffer[i - 1] = editerBuffer[i];
						 }
					}
				}
				term->edit.cursor--;
			}
		}

		/*
		* TODO: bounds checking on the buffer
		* TODO: command history
		*/
		void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){

			if(key == GLFW_KEY_LEFT_SHIFT || key == GLFW_KEY_RIGHT_SHIFT){
				if(action == GLFW_PRESS)
					control_shift = true;
				else
					control_shift = false;
			}

			if(action == GLFW_PRESS){
				if(control_shift){
					if(key == GLFW_KEY_A)
						add_char('A');
					if(key == GLFW_KEY_B)
						add_char('B');
					if(key == GLFW_KEY_C)
						add_char('C');
					if(key == GLFW_KEY_D)
						add_char('D');
					if(key == GLFW_KEY_E)
						add_char('E');
					if(key == GLFW_KEY_F)
						add_char('F');
					if(key == GLFW_KEY_G)
						add_char('G');
					if(key == GLFW_KEY_H)
						add_char('H');
					if(key == GLFW_KEY_I)
						add_char('I');
					if(key == GLFW_KEY_J)
						add_char('J');
					if(key == GLFW_KEY_K)
						add_char('K');
					if(key == GLFW_KEY_L)
						add_char('L');
					if(key == GLFW_KEY_M)
						add_char('M');
					if(key == GLFW_KEY_N)
						add_char('N');
					if(key == GLFW_KEY_O)
						add_char('O');
					if(key == GLFW_KEY_P)
						add_char('P');
					if(key == GLFW_KEY_Q)
						add_char('Q');
					if(key == GLFW_KEY_R)
						add_char('R');
					if(key == GLFW_KEY_S)
						add_char('S');
					if(key == GLFW_KEY_T)
						add_char('T');
					if(key == GLFW_KEY_U)
						add_char('U');
					if(key == GLFW_KEY_V)
						add_char('V');
					if(key == GLFW_KEY_W)
						add_char('W');
					if(key == GLFW_KEY_X)
						add_char('X');
					if(key == GLFW_KEY_Y)
						add_char('Y');
					if(key == GLFW_KEY_Z)
						add_char('Z');

					if(key == GLFW_KEY_0)
						add_char(')');
					if(key == GLFW_KEY_1)
						add_char('!');
					if(key == GLFW_KEY_2)
						add_char('@');
					if(key == GLFW_KEY_3)
						add_char('#');
					if(key == GLFW_KEY_4)
						add_char('$');
					if(key == GLFW_KEY_5)
						add_char('%');
					if(key == GLFW_KEY_6)
						add_char('^');
					if(key == GLFW_KEY_7)
						add_char('&');
					if(key == GLFW_KEY_8)
						add_char('*');
					if(key == GLFW_KEY_9)
						add_char('(');

					if(key == GLFW_KEY_COMMA)
						add_char('~');
					if(key == GLFW_KEY_SEMICOLON)
						add_char(':');
					if(key == GLFW_KEY_LEFT_BRACKET)
						add_char('{');
					if(key == GLFW_KEY_RIGHT_BRACKET)
						add_char('}');
					if(key == GLFW_KEY_BACKSLASH)
						add_char('|');
					if(key == GLFW_KEY_APOSTROPHE)
						add_char('~');

					if(key == GLFW_KEY_MINUS)
						add_char('_');
					if(key == GLFW_KEY_PERIOD)
						add_char('>');
					if(key == GLFW_KEY_EQUAL)
						add_char('+');
					if(key == GLFW_KEY_SLASH)
						add_char('?');

				}else{
					if(key == GLFW_KEY_A)
						add_char('a');
					if(key == GLFW_KEY_B)
						add_char('b');
					if(key == GLFW_KEY_C)
						add_char('c');
					if(key == GLFW_KEY_D)
						add_char('d');
					if(key == GLFW_KEY_E)
						add_char('e');
					if(key == GLFW_KEY_F)
						add_char('f');
					if(key == GLFW_KEY_G)
						add_char('g');
					if(key == GLFW_KEY_H)
						add_char('h');
					if(key == GLFW_KEY_I)
						add_char('i');
					if(key == GLFW_KEY_J)
						add_char('j');
					if(key == GLFW_KEY_K)
						add_char('k');
					if(key == GLFW_KEY_L)
						add_char('l');
					if(key == GLFW_KEY_M)
						add_char('m');
					if(key == GLFW_KEY_N)
						add_char('n');
					if(key == GLFW_KEY_O)
						add_char('o');
					if(key == GLFW_KEY_P)
						add_char('p');
					if(key == GLFW_KEY_Q)
						add_char('q');
					if(key == GLFW_KEY_R)
						add_char('r');
					if(key == GLFW_KEY_S)
						add_char('s');
					if(key == GLFW_KEY_T)
						add_char('t');
					if(key == GLFW_KEY_U)
						add_char('u');
					if(key == GLFW_KEY_V)
						add_char('v');
					if(key == GLFW_KEY_W)
						add_char('w');
					if(key == GLFW_KEY_X)
						add_char('x');
					if(key == GLFW_KEY_Y)
						add_char('y');
					if(key == GLFW_KEY_Z)
						add_char('z');

					if(key == GLFW_KEY_0 || key == GLFW_KEY_KP_0)
						add_char('0');
					if(key == GLFW_KEY_1 || key == GLFW_KEY_KP_1)
						add_char('1');
					if(key == GLFW_KEY_2 || key == GLFW_KEY_KP_2)
						add_char('2');
					if(key == GLFW_KEY_3 || key == GLFW_KEY_KP_3)
						add_char('3');
					if(key == GLFW_KEY_4 || key == GLFW_KEY_KP_4)
						add_char('4');
					if(key == GLFW_KEY_5 || key == GLFW_KEY_KP_5)
						add_char('5');
					if(key == GLFW_KEY_6 || key == GLFW_KEY_KP_6)
						add_char('6');
					if(key == GLFW_KEY_7 || key == GLFW_KEY_KP_7)
						add_char('7');
					if(key == GLFW_KEY_8 || key == GLFW_KEY_KP_8)
						add_char('8');
					if(key == GLFW_KEY_9 || key == GLFW_KEY_KP_9)
						add_char('9');

					if(key == GLFW_KEY_COMMA)
						add_char(',');
					if(key == GLFW_KEY_SEMICOLON)
						add_char(';');
					if(key == GLFW_KEY_LEFT_BRACKET)
						add_char('[');
					if(key == GLFW_KEY_RIGHT_BRACKET)
						add_char(']');
					if(key == GLFW_KEY_BACKSLASH)
						add_char('/');
					if(key == GLFW_KEY_KP_ADD)
						add_char('+');
					if(key == GLFW_KEY_APOSTROPHE)
						add_char('`');

					if(key == GLFW_KEY_MINUS || key == GLFW_KEY_KP_SUBTRACT)
						add_char('-');
					if(key == GLFW_KEY_PERIOD || key == GLFW_KEY_KP_DECIMAL)
						add_char('.');
					if(key == GLFW_KEY_KP_MULTIPLY)
						add_char('*');
					if(key == GLFW_KEY_EQUAL || key == GLFW_KEY_KP_EQUAL)
						add_char('=');
					if(key == GLFW_KEY_SLASH || key == GLFW_KEY_KP_DIVIDE)
						add_char('/');

					if(key == GLFW_KEY_ENTER || key == GLFW_KEY_KP_ENTER)
						parce_command();
					if(key == GLFW_KEY_SPACE)
						add_char(' ');
					if(key == GLFW_KEY_BACKSPACE)
						remove_char(false);
				}
			}
		}

		void parce_command(){
			/*
			* seperate command from buffer and prompt
			* our comman is from commandStart to editerLen
			*/
			bool command_statis = false;

			for(auto it = command_registry.begin(); it != command_registry.end(); it++){
				if(editerBuffer.compare(commandStart, (*it)->get_command_length(), ((*it)->get_command()).c_str(), (*it)->get_command_length()) == 0){
					int startpoint = 0;
					if(commandStart + (*it)->get_command_length() < editerBuffer.size()){
						startpoint = commandStart + (*it)->get_command_length() + 1;
					}else{
						startpoint = commandStart + (*it)->get_command_length();
					}
					(*it)->run(this, editerBuffer.substr(startpoint, std::string::npos));
					command_statis = true;
					break;
				}
			}

			if(!command_statis)
				write_string("\nError: no matching command.\n");

			write_string(prompt);
			commandStart = editerBuffer.size();
			struct nk_window* term = nk_window_find(ctx, Title);
			if(term)
				term->edit.cursor = commandStart;
		}

		bool is_int(std::string str){
			bool ret = true;
			for(int i = 0; i < str.size(); i++){
				if((str[i] < '0' || str[i] > '9') && str[i] != '-')
					ret = false;
			}
			return ret;
		}

		bool is_float(std::string str){
			bool ret = true;
			bool onepoint = true;
			for(int i = 0; i < str.size(); i++){
				if((str[i] < '0' || str[i] > '9') && str[i] != '-'){
					if(str[i] == '.' && onepoint)
						onepoint = false;
					else
						ret = false;
				}
			}
			return ret;
		}

		/*
		* Write a string to the buffer
		*/
		void write_string(const char* str){
			editerBuffer += str;
		}

		void write_string(std::string str){
			editerBuffer += str;
		}

		/*
		* Write a base 10 integer to the Terminal buffer
		*/
		void write_int(int num){
			editerBuffer += std::to_string(num);
		}

		/*
		* Clear the Terminal buffer
		*/
		void Terminal_clear(){
			editerBuffer.clear();

			struct nk_window* term = nk_window_find(ctx, Title);
			if(term)
				term->edit.cursor = 0;
			commandStart = 0;
		}
	};

};

#endif //TERMINAL_H