#ifndef GUI_RENDERER_H
#define GUI_RENDERER_H

#include <assert.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "nk_wrapper.h"
#include "shaders_api.h"
#include "glfw_window.h"

#ifdef __APPLE__
  #define NK_SHADER_VERSION "#version 150\n"
#else
  #define NK_SHADER_VERSION "#version 300 es\n"
#endif

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

class gui_renderer{
    struct GLFWwindow *win;
    struct nk_context* ctx;

    struct nk_buffer cmds;
    struct nk_draw_null_texture null;
    GLuint vbo, vao, ebo;
    GLuint prog;
    GLuint vert_shdr;
    GLuint frag_shdr;
    GLint attrib_pos;
    GLint attrib_uv;
    GLint attrib_col;
    GLint uniform_tex;
    GLint uniform_proj;
    GLuint font_tex;

    enum nk_anti_aliasing AntiAliasing;
    struct nk_font_atlas atlas;

    struct nk_glfw_vertex {
        float position[2];
        float uv[2];
        nk_byte col[4];
    };

public:
    gui_renderer(struct GLFWwindow *window, struct nk_context* context, enum nk_anti_aliasing = NK_ANTI_ALIASING_ON);
    ~gui_renderer();
    
    //=====================================
    // Nothing is actually drawn until this function
    // Previously we populated the command buffer and
    // this function runs through and executes that buffer
    // This draws the gui
    //=====================================
    void gui_render();
};

#endif //GUI_RENDERER_H