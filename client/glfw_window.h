#ifndef GLFW_WINDOW_H
#define GLFW_WINDOW_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "global_scene_map.h"

#include "nk_wrapper.h"

#ifndef INPUT_TEXT_MAX
#define INPUT_TEXT_MAX 256
#endif

enum nk_glfw_init_state{
    NK_GLFW3_DEFAULT=0,
    NK_GLFW3_INSTALL_CALLBACKS
};

static struct nk_glfw {
    GLFWwindow *win;
	struct nk_vec2 scroll;
    int text_len;
    char text[INPUT_TEXT_MAX];
} glfw;

//=====================================
// The GLFW window and nuklear context
// are closely related. We build them
// seperately to get pointers but destroy
// them together.
//=====================================
GLFWwindow* window_init(int w, int h, const char* title);
NK_API struct nk_context* context_init(GLFWwindow *win, enum nk_glfw_init_state init_state);
void window_destroy(struct nk_context* ctx, GLFWwindow* win);

//=====================================
// Callbacks for mouse scroll and text queue
// that can be sent to nuklear. To use set
// init_state to NK_GLFW3_INSTALL_CALLBACKS
//=====================================
NK_API void char_callback(GLFWwindow *win, unsigned int codepoint);
NK_API void scroll_callback(GLFWwindow *win, double xoff, double yoff);

//=====================================
// Callback functions we provide to the
// nuklear internals to allow copy/paste
//=====================================
NK_INTERN void glfw_clipbard_paste(nk_handle usr, struct nk_text_edit *edit);
NK_INTERN void glfw_clipbard_copy(nk_handle usr, const char *text, int len);

//=====================================
// Here we bulk capture GLFW window input
// and send it to the nuklear context
//=====================================
NK_API void capture_input(struct GLFWwindow *win, struct nk_context *ctx);

#endif //GLFW_WINDOW_H