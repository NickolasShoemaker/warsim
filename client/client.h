#ifndef CLIENT_H
#define CLIENT_H

#include <chrono>
#include <unordered_map>
#include <string>
#include <memory>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "nk_wrapper.h"
#include "glfw_window.h"

#include "global_scene_map.h"
#include "texture_atlas.h"

class client{
	texture_atlas* atlas;

	std::unordered_map<std::string, std::shared_ptr<scene> > map;
	static std::shared_ptr<scene> current;
public:
	GLFWwindow* win;
	struct nk_context* ctx;

	client(){
		win = window_init(800, 600, "War Sim");
    	ctx = context_init(win, NK_GLFW3_INSTALL_CALLBACKS);
    	atlas = new texture_atlas("./resourses");

    	// because c++ is weird we need global functions to pass the events
	    // to the coresponding member of the current scene
	    glfwSetKeyCallback(win, global_key_callback);
	    glfwSetMouseButtonCallback(win, global_mouse_button_callback);
	    glfwSetScrollCallback(win, global_scroll_callback);
	    glfwSetCursorPosCallback(win, global_cursor_position_callback);
	}
	~client(){
		delete atlas;
		window_destroy(ctx, win);
	}
	
	// scene functions
	void register_scene(std::string title, std::shared_ptr<scene> scn){	map[title] = scn;}
	void set_current_scene(std::string title){	current = map[title];}
	std::shared_ptr<scene> get_current_scene() const {	return current;	}

	void operator()(){
		std::chrono::system_clock::time_point t1 = std::chrono::system_clock::now();

	    while(!glfwWindowShouldClose(win)){
	        // Input
	        glfwPollEvents();
	        capture_input(win, ctx);

	        current->render();
	        glfwSwapBuffers(win); 

	        // FPS counter for testing
	        /*std::chrono::system_clock::time_point t2 = std::chrono::system_clock::now();
	        std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
	        printf("Seconds is %f\n", 1/time_span.count());
	        t1 = t2;*/
	    }
	}

	static void global_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	    current->key_callback(window, key, scancode, action, mods);
	}
	static void global_mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	    current->mouse_button_callback(window, button, action, mods);
	}
	static void global_scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
	    current->scroll_callback(window, xoffset, yoffset);
	}
	static void global_cursor_position_callback(GLFWwindow* window, double xpos, double ypos){
	    current->cursor_position_callback(window, xpos, ypos);
	}

};

#endif //CLIENT_H