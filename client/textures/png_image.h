#ifndef PNG_IMAGE_H
#define PNG_IMAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#include <exception>
#include <utility>
#include <unordered_map>
#include <vector>

#include <string>

#include <GL/glew.h>
#include <png.h>

#include "logger.h"

/*
* A wrapper class for libpng
*/
class png_image{
	/*
	* Error opening PNG
	*/
	class not_png : public std::exception{
		const char* m_message;
	public:
		not_png(const char* a_message):m_message(a_message){}
		const char* what(){
			return m_message;
		}
	};
	/*
	* Error opening PNG
	* TODO: buffer is fixed at 128, not dynamic
	*/
	class png_out_of_bounds : public std::exception{
		char m_buffer[128];
	public:
		png_out_of_bounds(const char* a_message, int a_index, int a_bounds){
			snprintf(m_buffer, 128, "%s: index %d < %d", a_message, a_index, a_bounds);
		}
		const char* what(){
			return m_buffer;
		}
	};
	/*
	* Error create the structures for reading a PNG
	*/
	class bad_read_struct : public std::exception{
		const char* m_message;
	public:
		bad_read_struct(const char* a_message):m_message(a_message){}
		const char* what(){
			return m_message;
		}
	};
	/*
	* Error create the structures for writting a PNG
	*/
	class bad_write_struct : public std::exception{
		const char* m_message;
	public:
		bad_write_struct(const char* a_message):m_message(a_message){}
		const char* what(){
			return m_message;
		}
	};
public:
	/*
	* The name of the image
	*/
	std::string m_title;

	/*
	* One dimentional array of pixel bytes
	*/
	png_byte* m_image_data;

	/*
	* Two dimentional array of pixel bytes
	*/
	png_byte** m_rows;

	/*
	* The width and height of the PNG in pixels
	*/
	png_uint_32 m_height;
	png_uint_32 m_width;

	/*
	* RGB or RGBA
	*/
	GLint m_format;
	int m_color_type;

	int m_bit_depth;

	/*
	* The width of the PNG in bytes
	*/
	int m_rowbytes;

	/*
	* The size of a pixel in bytes
	*/
	int m_bytes_per_pixel;

	std::unordered_map<std::string, std::string> m_tExt;

	logger png_logger;

	/*
	* Load png from file
	*/
	png_image(const char* a_file_name){
		FILE* fp;
	    png_byte header[8];
		png_structp png_ptr = nullptr;
		png_infop info_ptr = nullptr;
		png_infop end_info = nullptr;
		m_title = std::string(a_file_name);

		try{
			fp = fopen(a_file_name, "rb");
		    if(!fp){
		        png_logger(__FILE__,__LINE__) << "ERROR: could not load texture at location " << a_file_name << " no such location found.\n";
		        png_logger.log();
				std::terminate();
		    }

			fread(header, 1, 8, fp);
			if(png_sig_cmp(header, 0, 8))
				throw(new not_png(a_file_name));    			    
		
			png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
			if(!png_ptr)
			    throw(new bad_read_struct("png_create_read_struct returned 0."));

			info_ptr = png_create_info_struct(png_ptr);
			if(!info_ptr)
			    throw(new bad_read_struct("png_create_info_struct returned 0."));

			end_info = png_create_info_struct(png_ptr);
			if(!end_info)
			    throw(new bad_read_struct("png_create_info_struct returned 0."));

			if(setjmp(png_jmpbuf(png_ptr)))
				throw(new bad_read_struct("libpng error."));
			
			png_init_io(png_ptr, fp);			// init png reading
			png_set_sig_bytes(png_ptr, 8);		// let libpng know you already read the first 8 bytes
			png_read_info(png_ptr, info_ptr);	// read all the info up to the image data

			png_get_IHDR(png_ptr, info_ptr, &m_width, &m_height, &m_bit_depth, &m_color_type, NULL, NULL, NULL);

			if(m_bit_depth != 8){
		        png_logger(__FILE__,__LINE__) << "ERROR:" << a_file_name << ": Unsupported bit depth " << m_bit_depth << ".  Must be 8.\n";
				png_logger.log();
				std::terminate();
		    }

		    switch(m_color_type){
			    case PNG_COLOR_TYPE_RGB:
			        m_format = GL_RGB;
			        break;
			    case PNG_COLOR_TYPE_RGB_ALPHA:
			        m_format = GL_RGBA;
			        break;
			    default:
			        png_logger(__FILE__,__LINE__) << "ERROR: " << a_file_name << ": Unknown libpng color type " << m_color_type << ".\n";
			        png_logger.log();
		    }

		    png_read_update_info(png_ptr, info_ptr);				// Update the png info struct.
			m_rowbytes = png_get_rowbytes(png_ptr, info_ptr);		// Row size in bytes.
			m_rowbytes += 3 - ( (m_rowbytes - 1) % 4);				// glTexImage2d requires rows to be 4-byte aligned

			png_text* text_ptr;
			int num_comments = png_get_text(png_ptr, info_ptr, &text_ptr, NULL);

			if(num_comments > 0){
				png_logger() << "INFO: Image " << m_title << " has tExt of \n";
		        for(int i = 0; i < num_comments; i++){
		            m_tExt.emplace(text_ptr[i].key, text_ptr[i].text);
		            png_logger() << "{" << text_ptr[i].key << " : " << text_ptr[i].text << "}\n";
		        }
		        png_logger.log();
		    }

		    m_image_data = new png_byte[m_rowbytes * m_height]; //sizeof(png_byte)+15
		    if(!m_image_data)
		    	throw(new bad_read_struct("could not allocate memory for PNG image data"));

		    m_rows = new png_byte* [m_height]; //sizeof(png_byte*)
		    if(!m_rows){
		    	delete [] m_image_data;
		    	throw(new bad_read_struct("could not allocate memory for PNG row pointers"));
		    }

		    // set the individual row_pointers to point at the correct offsets of image_data
		    for(png_uint_32 i = 0; i < m_height; i++)
		        m_rows[m_height - 1 - i] = m_image_data + i * m_rowbytes;

		    // read the png into image_data through row_pointers
		    png_read_image(png_ptr, m_rows);

		    // clean up
		    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		    fclose(fp);

		}catch(not_png* except){
			png_logger(__FILE__,__LINE__) << "PNG ERROR: " << except->what() << " is not a png.\n";
			png_logger.log();
			fclose(fp);
		}catch(bad_read_struct* except){
			png_logger(__FILE__,__LINE__) << "PNG ERROR: " << except->what() << " is not a png.\n";
			png_logger.log();
			png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
			fclose(fp);
		}
	}

	/*
	* Create a blank image with height x with
	*/
	png_image(png_uint_32 a_height, png_uint_32 a_width):m_height(a_height),m_width(a_width){
		m_title = "";
		m_bytes_per_pixel = 4;
	    m_image_data = new png_byte[a_height * a_width * m_bytes_per_pixel];
	    m_rows = new png_bytep[a_height];

	    m_rowbytes = m_bytes_per_pixel * a_width;
	    m_bit_depth = 8;
	    m_color_type = PNG_COLOR_TYPE_RGB_ALPHA;
	    m_format = GL_RGBA;

	    //png_error(png_ptr, "Image is too tall to process in memory");
	    if(a_height > PNG_UINT_32_MAX / sizeof(png_bytep)){
	        png_logger(__FILE__,__LINE__) << "PNG ERROR: Image is too tall to process in memory\n";
	        png_logger.log();
	    }

	    for(png_uint_32 k = 0; k < a_height; k++)
	        m_rows[k] = m_image_data + k * a_width * m_bytes_per_pixel;

	    // clear memory so we don't get garbage in our image
	    memset(m_image_data, 0, a_height * a_width * m_bytes_per_pixel);
	}

	/*
	* Do not copy an image by accident
	* If we need to we can use the copy_data function
	*/
	png_image(png_image&) = delete;

	~png_image(){
		delete [] m_image_data;
		delete [] m_rows;
	}
	
	/*
	* Write png to file. Make sure you make all your changes first.
	*/
	void write(const char* a_file_name){
		FILE *fp;
    	png_structp png_ptr;
    	png_infop info_ptr;

    	try{

		    fp = fopen(a_file_name, "wb");
		    if(!fp)
		    	throw(new not_png(a_file_name));

		    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		    if(!png_ptr)
		    	throw(new bad_write_struct("failed to create png write struct."));

		    info_ptr = png_create_info_struct(png_ptr);
		    if(!info_ptr)
				throw(new bad_write_struct("failed to create png info struct."));

		    if(setjmp(png_jmpbuf(png_ptr)))
		    	throw(new bad_write_struct("libpng error."));


	    	png_init_io(png_ptr, fp);
	    	png_set_IHDR(png_ptr, info_ptr, m_width, m_height, m_bit_depth, m_color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	    	if(!m_tExt.empty()){
		        png_text* text_ptr = new png_text[m_tExt.size()];
		        int i = 0;
		        for(auto it = m_tExt.begin(); it != m_tExt.end(); it++){
		            text_ptr[i].key = const_cast<char*>(it->first.c_str());
		            text_ptr[i].text = const_cast<char*>(it->second.c_str());
		            text_ptr[i].compression = PNG_TEXT_COMPRESSION_NONE;
		            i++;
		        }
		        png_set_text(png_ptr, info_ptr, text_ptr, m_tExt.size());
		        delete [] text_ptr;
		    }

	    	png_write_info(png_ptr, info_ptr);
	    	png_write_image(png_ptr, m_rows);
	    	png_write_end(png_ptr, info_ptr);

	    	png_destroy_write_struct(&png_ptr, &info_ptr);
	    	fclose(fp);
	    }catch(not_png* except){
	    	png_logger() << "PNG ERROR:" << except->what() << " could not be opened for writing\n";
	    	png_logger.log();
	    }catch(bad_write_struct* except){
	    	png_logger() << "PNG ERROR:" << except->what() << std::endl;
	    	png_logger.log();
	        png_destroy_write_struct(&png_ptr, &info_ptr);
	        fclose(fp);
	    }
    }

    /*
    * Create an OpenGL texture from the PNG data
    * We can freely let png_image go out of scope without effecting the texture
    */
	GLuint GLtexture(){
		GLuint texture;

    	glGenTextures(1, &texture);
    	glBindTexture(GL_TEXTURE_2D, texture);
    	glTexImage2D(GL_TEXTURE_2D, 0, m_format, m_width, m_height, 0, m_format, GL_UNSIGNED_BYTE, m_image_data);
    	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
    	return texture;
	}

	/*
	* Move through source pixel by pixel and copy at optional offset into destination
	*/
	void copy_data(std::shared_ptr<png_image> source, short x = 0, short y = 0){
		try{
			// can source fit in dest			
			if(x + source->m_width > m_width)
				throw(new png_out_of_bounds("attempting to copy data from png with offset x greater then png width", x + source->m_width, m_width));

			if(y + source->m_height > m_height)
				throw(new png_out_of_bounds("attempting to copy data from png with offset y greater then png height", y + source->m_height, m_height));

			if(source->m_format == this->m_format){
				// same
				png_logger() << "INFO: Writing image into image of same format\n";
				png_logger.log();
				for(int height = 0; height < source->m_height; height++){
					for(int width = 0; width < source->m_rowbytes; width++){
						(*this)(y + height, x * m_bytes_per_pixel + width) = (*source)(height, width);
					}
				}
			}else if(source->m_format == GL_RGB && this->m_format == GL_RGBA){
				// rgb to rgba
				png_logger() << "INFO: Writing rgb image into a rgba\n";
				png_logger.log();
				for(int height = 0; height < source->m_height; height++){
					for(int width = 0; width < source->m_width; width++){
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 0) = (*source)(height, (width * source->m_bytes_per_pixel) + 0);
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 1) = (*source)(height, (width * source->m_bytes_per_pixel) + 1);
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 2) = (*source)(height, (width * source->m_bytes_per_pixel) + 2);
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 3) = 1;
					}
				}
			}else{
				// rgba to rgb
				png_logger() << "INFO: Writing rgba image into a rgb\n";
				png_logger.log();
				for(int height = 0; height < source->m_height; height++){
					for(int width = 0; width < source->m_width; width++){
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 0) = (*source)(height, (width * source->m_bytes_per_pixel) + 0);
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 1) = (*source)(height, (width * source->m_bytes_per_pixel) + 1);
						(*this)(y + height, (x * m_bytes_per_pixel) + (width * m_bytes_per_pixel) + 2) = (*source)(height, (width * source->m_bytes_per_pixel) + 2);
					}
				}
			}
		}catch(png_out_of_bounds* except){
			png_logger(__FILE__,__LINE__) << "PNG ERROR:" << except->what() << std::endl;
	    	png_logger.log();
		}
	}

	/*
	* Index into PNG data as one dimention
	*/
	png_byte& operator[](int a_index){
		try{
			if(a_index >= m_rowbytes * m_height)
				throw(new png_out_of_bounds("index out of png memory range", a_index, m_rowbytes * m_height));

			return m_image_data[a_index];
		}catch(png_out_of_bounds* except){
			png_logger(__FILE__,__LINE__) << "PNG ERROR:" << except->what() << std::endl;
	    	png_logger.log();
		}
	}

	/*
	* Index into PNG data as two dimentions
	*/
	png_byte& operator()(int a_y, int a_x){
		try{
			if(a_x >= m_rowbytes)
				throw(new png_out_of_bounds("x is larger than row bytes.", a_x, m_rowbytes));

			if(a_y >= m_height)
				throw(new png_out_of_bounds("y is larger than height.", a_y, m_height));

			return m_rows[a_y][a_x];
		}catch(png_out_of_bounds* except){
			png_logger(__FILE__,__LINE__) << "PNG ERROR:" << except->what() << std::endl;
	    	png_logger.log();
		}
	}
};

#endif //PNG_IMAGE_H