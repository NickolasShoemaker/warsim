#ifndef TEXTURE_ATLAS_H
#define TEXTURE_ATLAS_H

#include <GL/glew.h>

#include "png_image.h"
#include "texture_atlas_node_map.h"

#include "logger.h"

#include <boost/filesystem.hpp>

#include <memory>
#include <unordered_map>
#include <algorithm>

#include <cstring>
#include <cassert>

/*
* Warning: Only PNGs with alpha can be added to atlas or else they will be mangles.
* Perhaps a conversion could be added in the future.
*
* We will want a GLtexture and sampler to send to the shaders
*/
class texture_atlas{
	//std::unordered_map<short, tExt_entry> map; // a map of entries by block id
	//GLuint texture;

	logger ta_logger;

	// texture atlas lookup
	std::vector<std::array<uint16_t, 4>> coords_lookup;	//4 coords per texture
	std::vector<std::array<uint16_t, 6>> index_lookup;		//6 sides per voxel
	std::unordered_map<std::string, uint16_t> block_dictionary; //name to id

	std::unordered_map<uint16_t, uint16_t> server_id;	//match server id's with client id's
public:
	/*
	* A texture atlas is given a directory where it will either build an atlas from
	* the stored resourses or loading an already created atlas
	*/
	texture_atlas(const char* dir){
		ta_logger.set_file("./log/main.log");

		boost::filesystem::path directory_path(dir);
		if(boost::filesystem::is_directory(directory_path)){
			if(boost::filesystem::exists(directory_path / "texture_atlas.png")){
				ta_logger() << "INFO: There is alread a texture atlas in this directory, we should load it.\n";
				ta_logger.log();
				load((directory_path / "texture_atlas.png").string().c_str());
			}else{
				build_atlas(directory_path);
			}
		}else{
			ta_logger(__FILE__, __LINE__) << "ERROR: could not open directory " << dir << " to load texture atlas.\n";
			ta_logger.log();
		}
	}
	
	/*
	* Loop through directory and build texture atlas from pngs
	*/
	void build_atlas(boost::filesystem::path directory_path){
		atlas_node_map node_map(&block_dictionary, &index_lookup, &coords_lookup);
		for(boost::filesystem::directory_iterator it(directory_path); it != boost::filesystem::directory_iterator(); it++){
			if(it->path().extension() == ".png" || it->path().extension() == ".PNG"){
				ta_logger() << "INFO: Reading png at location " << it->path().string().c_str() << std::endl;
				ta_logger.log();

				// we can pull off extension using .stem() then parce the name for texture info

				node_map.add_unplaced_image(std::shared_ptr<png_image>(new png_image(it->path().string().c_str())));
			}
		}
		node_map.build();
	}

	/*
	* Load texture atlas from file
	*/
	void load(const char* file){
		png_image atlas(file);

		ta_logger() << "parcing block_dictionary\n";
		ta_logger.log();

		// load the block dictionary
		std::string block_string = atlas.m_tExt["block_lookup"];
		std::string block_name;
		std::string block_id;
		bool name = true;
		for(int i = 0; i < block_string.size(); i++){
			char c = block_string[i];
			if(c == ':'){
				name = false;
			}else if(c == ','){
				block_dictionary.emplace(block_name, std::stoi(block_id));
				block_name.clear();
				block_id.clear();
				name = true;
			}else if(name){
				block_name += c;
			}else{
				block_id += c;
			}
		}

		ta_logger() << "parcing block index\n";
		ta_logger.log();
		
		/*
		* load index lookup
		*/
		std::string index_string = atlas.m_tExt["index_lookup"];
		size_t end;
		size_t start = index_string.find('[');

		std::string temp_string;

		while(start + 1 < index_string.size() - 1 && start != std::string::npos){
			std::array<uint16_t, 6> temp;

			start++;
			end = index_string.find(',', start);
			if(end == std::string::npos){
				ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
				ta_logger.log();
				break;
			}
			temp_string = index_string.substr(start, end - start);
			temp[0] = std::stoi(temp_string);

			for(int i = 1; i < 5; i++){
				start = end + 1;
				end = index_string.find(',', start);
				if(start == std::string::npos || end == std::string::npos){
					ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
					ta_logger.log();
					break;
				}
				temp_string = index_string.substr(start, end - start);
				temp[i] = std::stoi(temp_string);				
			}

			start = end + 1;
			end = index_string.find(']', start);
			if(start == std::string::npos || end == std::string::npos){
				ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
				ta_logger.log();
				break;
			}
			temp_string = index_string.substr(start, end - start);
			temp[5] = std::stoi(temp_string);

			index_lookup.push_back(temp);

			start = index_string.find('[', end + 1);
		}

		ta_logger() << "parcing coords lookup\n";
		ta_logger.log();

		/*
		* load coords lookup
		*/
		std::string coords_string = atlas.m_tExt["coords_lookup"];
		start = coords_string.find('[');

		while(start + 1 < coords_string.size() - 1  && start != std::string::npos){
			std::array<uint16_t, 4> temp;

			start++;
			end = coords_string.find(',', start);
			if(end == std::string::npos){
				ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
				ta_logger.log();
				break;
			}
			temp_string = coords_string.substr(start, end - start);
			temp[0] = std::stoi(temp_string);

			for(int i = 1; i < 3; i++){
				start = end + 1;
				end = coords_string.find(',', start);
				if(start == std::string::npos || end == std::string::npos){
					ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
					ta_logger.log();
					break;
				}
				temp_string = coords_string.substr(start, end - start);
				temp[i] = std::stoi(temp_string);		
			}

			start = end + 1;
			end = coords_string.find(']', start);
			if(start == std::string::npos || end == std::string::npos){
				ta_logger(__FILE__, __LINE__) << "ERROR: parcing index string for texture_atlas.\n";
				ta_logger.log();
				break;
			}
			temp_string = coords_string.substr(start, end - start);
			temp[3] = std::stoi(temp_string);

			coords_lookup.push_back(temp);

			start = coords_string.find('[', end + 1);
		}

	}
};

#endif //TEXTURE_ATLAS_H