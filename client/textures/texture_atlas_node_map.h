#ifndef TEXTURE_ATLAS_MAP_H
#define TEXTURE_ATLAS_MAP_H

#include "png_image.h"

#include "logger.h"

#include <memory>
#include <unordered_map>
#include <algorithm>
#include <bitset>

#include <cstring>
#include <cassert>

/*
* 
* Atlas node map
* 
*/
class atlas_node_map{
	/*
	* 
	* Atlas node
	* 
	*/
	class atlas_node{
	public:
		/*
		* UV coords for when we build the sampler
		* We set these after all the nodes are placed because if
		* the map is grown it could invalide the coords.
		*
		* The top left of the texture
		*/
		short m_x;
		short m_y;

		/*
		* The height, width, and image data for the image this node represents
		*/
		unsigned long m_width;
		unsigned long m_height;
		std::shared_ptr<png_image> m_image;

		/*
		* The node adjacent to the bottom and right in the node map
		*/
		std::shared_ptr<atlas_node> m_bottom;
		std::shared_ptr<atlas_node> m_right;

		/*
		* Create a node with data
		*/
		atlas_node(unsigned long a_width, unsigned long a_height, std::shared_ptr<png_image> a_image){
			m_width = a_width;
			m_height = a_height;
			m_image = a_image;
			m_bottom = nullptr;
			m_right = nullptr;
		}
		/*
		* Create a node without data
		*/
		atlas_node(unsigned long a_width, unsigned long a_height){
			m_width = a_width;
			m_height = a_height;
			m_image = nullptr;
			m_bottom = nullptr;
			m_right = nullptr;
		}
	};
	
	logger node_map_logger;

	/*
	* A lot of the algorithms for node maps are recursive so we have a root
	*/
	std::shared_ptr<atlas_node> root;

	/*
	* At first this is the size of the root node then of the whole map as
	* the nodes a split. We have to increase these if we grow the map
	*/
	short node_map_width;
	short node_map_height;

	/*
	* Recursively move right and down until an empty node of the
	* right size is found or return nullptr
	*/
	std::shared_ptr<atlas_node> findfit(std::shared_ptr<atlas_node> a_node, short a_width, short a_height){
		// we have left map
		if(a_node == nullptr)
			return nullptr;

		// The node we are at has data so we need to keep moving
		if(a_node->m_image != nullptr){

			// See if there is a free node to the right
			// If not move down
			std::shared_ptr<atlas_node> fit = findfit(a_node->m_right, a_width, a_height);
			if(fit != nullptr){
				return fit;
			}else{
				return findfit(a_node->m_bottom, a_width, a_height);
			}

		// This node is empty. Send it or nullptr
		}else if(a_width <= a_node->m_width && a_height <= a_node->m_height){
			return a_node;
		}else{
			return nullptr;
		}
	}

	/*
	* Now we have a node of the right size we can set the data
	* set the node to the right size and add the exess to the right
	* and bottom. The right ends up being larger.
	*/
	void split(std::shared_ptr<atlas_node> a_node, std::shared_ptr<png_image> a_image){
		assert(a_image != nullptr);
		a_node->m_image = a_image;

		if(a_node->m_width - a_image->m_width > 0){
			a_node->m_right = std::shared_ptr<atlas_node>(new atlas_node(a_node->m_width - a_image->m_width, a_node->m_height));
		}else{
			a_node->m_right = nullptr;
		}

		if(a_node->m_height - a_image->m_height > 0){
			a_node->m_bottom = std::shared_ptr<atlas_node>(new atlas_node(a_image->m_width, a_node->m_height - a_image->m_height));
		}else{
			a_node->m_bottom = nullptr;
		}

		// Adjust the node to the right size
		a_node->m_width = a_image->m_width;
		a_node->m_height = a_image->m_height;
	}

	/*
	* Increase to the far right or far bottom
	*/
	void grow(std::shared_ptr<png_image> a_image){
		assert(a_image != nullptr);

		if(node_map_width >= node_map_height){ // add height keep width
			std::shared_ptr<atlas_node> bottom_most = root;
			while(bottom_most->m_bottom){
				bottom_most = bottom_most->m_bottom;
			}
			bottom_most->m_bottom = std::shared_ptr<atlas_node>(new atlas_node(node_map_width, a_image->m_height));
			node_map_height += a_image->m_height;
			split(bottom_most->m_bottom, a_image);
			node_map_logger() << "INFO: growing down\n";
			node_map_logger.log();
		}else{
			std::shared_ptr<atlas_node> right_most = root;
			while(right_most->m_right){
				right_most = right_most->m_right;
			}
			right_most->m_right = std::shared_ptr<atlas_node>(new atlas_node(a_image->m_width, node_map_height));
			node_map_width += a_image->m_width;
			split(right_most->m_right, a_image);
			node_map_logger() << "INFO: growing down\n";
			node_map_logger.log();
		}
	}

	/*
	* Transvers the map and set the x,y for the top left of each texture
	*/
	void setXY(std::shared_ptr<atlas_node> a_node, std::shared_ptr<png_image> img, short a_x, short a_y){
		if(a_node == nullptr)
			return;

		node_map_logger() << "INFO: Setting node x to " << a_x << " and y to " << a_y << std::endl;
		node_map_logger.log();

		a_node->m_x = a_x;
		a_node->m_y = a_y;

		if(a_node->m_image != nullptr)
			img->copy_data(a_node->m_image, a_x, a_y);

		setXY(a_node->m_bottom, img, a_node->m_x, a_node->m_y + a_node->m_height);
		setXY(a_node->m_right, img, a_node->m_x + a_node->m_width, a_node->m_y);
	}

	/*
	* Add each image to the node map and either split an empty node or grow the map
	*/
	void add_to_map(std::shared_ptr<png_image> a_image){
		assert(a_image != nullptr);

		node_map_logger() << "INFO: Adding node " << a_image->m_width << "x" << a_image->m_height << " to map\n";
		node_map_logger.log();

		std::shared_ptr<atlas_node> fit = findfit(root, a_image->m_width, a_image->m_height);
		if(fit == nullptr){
			grow(a_image);
		}else{
			split(fit, a_image);
		}
	}

	/*
	* Build the lookup tables so we can find the texture in the atlas
	*/
	void build_lookup(std::shared_ptr<atlas_node> a_node){
		if(a_node == nullptr)
			return;

		if(a_node->m_image != nullptr){
			std::bitset<6> sides = parce_texture_name(a_node->m_image->m_title);
			std::array<uint16_t, 4> coords;
			coords[0] = a_node->m_x;
			coords[1] = a_node->m_y;
			coords[2] = a_node->m_width;
			coords[3] = a_node->m_height;
			add_lookup_index(a_node->m_image->m_title, coords, sides);
		}

		build_lookup(a_node->m_bottom);
		build_lookup(a_node->m_right);
	}

	/*
	* Modifies input name to be the name of the block and returns bitset
	* corresponding to the following code:
	*
	* textures can be saved in the format [block_name].sides.png
	* where sides can be [0-5]+ or nothing
	*
	* sides = [top,bottom,left,right,front,back,side,x,y,z]
	*
	* If block_name is found again the current entry is added to or overwritten
	* if sides is not defined all sides will be set to the current texture
	*
	* When reading textures if a side is not defined the first texture is used
	*/
	std::bitset<6> parce_texture_name(std::string& a_block_name){
		std::bitset<6> sides;

		// name is everything between last / or start and . or end
		//printf("finding last / in %s\n", a_block_name.c_str());
		size_t start = a_block_name.find_last_of('/');
		if(start == std::string::npos)
			start = 0;

		//printf("finding first . in %s\n", a_block_name.c_str());
		size_t end = a_block_name.find_first_of('.', start);
		if(end == std::string::npos)
			end = a_block_name.size() - 1;

		std::string name = a_block_name.substr(start + 1, end - (start + 1));
		node_map_logger() << "INFO: block name is " << name << std::endl;
		node_map_logger.log();

		// find last of ., if end we are done
		//printf("finding last . in %s\n", a_block_name.c_str());
		size_t parce_end = a_block_name.find_last_of('.');
		if(parce_end == std::string::npos)
			parce_end = a_block_name.size() - 1;

		if(parce_end == end){ //no input so us for every side
			sides.set();
		}else{ // parce it
			std::string parce = a_block_name.substr(end + 1, parce_end - (end + 1));
			//printf("parce string is %s\n", parce.c_str());

			if(parce == "top"){
				sides.set(0, true);
			}else if(parce == "bottom"){
				sides.set(1, true);
			}else if(parce == "right"){
				sides.set(2, true);
			}else if(parce == "left"){
				sides.set(3, true);
			}else if(parce == "front"){
				sides.set(4, true);
			}else if(parce == "back"){
				sides.set(5, true);
			}else if(parce == "side"){
				sides.set(2, true);
				sides.set(3, true);
				sides.set(4, true);
				sides.set(5, true);
			}else if(parce == "z"){
				sides.set(4, true);
				sides.set(5, true);
			}else if(parce == "x"){
				sides.set(2, true);
				sides.set(3, true);
			}else if(parce == "y"){
				sides.set(0, true);
				sides.set(1, true);
			}else{
				node_map_logger(__FILE__,__LINE__) << "ERROR: Unrecognized texture option " << parce << " for file with name " << name << ", see texture README for more info.\n";
				node_map_logger.log();
			}
		}

		a_block_name = name;
		//printf("block name has been set to %s\n", a_block_name.c_str());
		return sides;
	}

	/*
	* Build the lookup tables nessisary to find the textures in the atlas
	*
	* TODO: debug.png should map to 0 so if we are missing a texture it will be the debug texture by default
	*/
	void add_lookup_index(std::string a_block_name, std::array<uint16_t, 4> a_coords, std::bitset<6> a_sides){
		// no new side to add
		if(a_sides.count() == 0){
			node_map_logger() << "INFO: Block " << a_block_name << " has no side so skipping it.\n";
			node_map_logger.log();
			return;
		}

		node_map_logger() << "INFO: Adding " << a_block_name << " to texture lookup\n";
		node_map_logger.log();

		// add this texture to the map
		int coord_index = m_coords_lookup->size();
		m_coords_lookup->push_back(a_coords);

		// do we have a block for this yet
		int index;
		if(m_block_lookup->count(a_block_name) == 0){
			std::array<uint16_t, 6> sides;
			sides.fill(0);
			index = m_index_lookup->size();
			m_index_lookup->push_back(sides);
			m_block_lookup->emplace(a_block_name, index);
			node_map_logger() << "INFO: Adding " << a_block_name << " block dictionary, set to index " << index << std::endl;
			node_map_logger.log();
		}else{
			// look up sides entry
			index = m_block_lookup->at(a_block_name);
			node_map_logger() << "INFO: " << a_block_name <<  " is at index " << index << std::endl;
			node_map_logger.log();
		}
	
		// set this texture for indicated sides
		for(int i = 0; i < a_sides.size(); i++){
			if(a_sides[i])
				m_index_lookup->at(index).at(i) = coord_index;
		}
		node_map_logger() << "INFO: Finished setting sides\n";
		node_map_logger.log();
	}

	std::vector<std::shared_ptr<png_image>> unplaced_nodes;

	std::unordered_map<std::string, uint16_t>* m_block_lookup;
	std::vector<std::array<uint16_t, 6>>* m_index_lookup;
	std::vector<std::array<uint16_t, 4>>* m_coords_lookup;

public:
	atlas_node_map(std::unordered_map<std::string, uint16_t>* a_block_lookup, std::vector<std::array<uint16_t, 6>>* a_index_lookup, std::vector<std::array<uint16_t, 4>>* a_coords_lookup){
		node_map_logger.set_file("./log/main.log");
		m_block_lookup = a_block_lookup;
		m_index_lookup = a_index_lookup;
		m_coords_lookup = a_coords_lookup;
	}
	~atlas_node_map(){}

	/*
	* We pass in an image and a way to identify it once it has been placed
	*/
	void add_unplaced_image(std::shared_ptr<png_image> image){
		unplaced_nodes.push_back(image);
		node_map_logger() << "INFO: Setting unplaced image\n";
		node_map_logger.log();
	}

	/*
	* Walk tree and build a png keeping track of node locations for tExt
	*/
	void build(){
		if(!unplaced_nodes.empty()){

			// sort the nodes
			std::sort(unplaced_nodes.begin(), unplaced_nodes.end(), [](std::shared_ptr<png_image> a_left, std::shared_ptr<png_image> a_right){
				unsigned long left_longest = (a_left->m_width > a_left->m_height) ? a_left->m_width : a_left->m_height;
				unsigned long right_longest = (a_right->m_width > a_right->m_height) ? a_right->m_width : a_right->m_height;

				return left_longest > right_longest;
			});

			// set width and hight higher than largest image
			node_map_width = unplaced_nodes.front()->m_width;
			node_map_height = unplaced_nodes.front()->m_height;

			root = std::shared_ptr<atlas_node>(new atlas_node(node_map_width, node_map_height));
			node_map_logger() << "INFO: Set root node as " << node_map_width << " x " << node_map_height << std::endl;
			node_map_logger.log();

			for(auto it = unplaced_nodes.begin(); it < unplaced_nodes.end(); it++){
				add_to_map(*it);
			}

			// create png node_map_width X node_map_height
			std::shared_ptr<png_image> image = std::shared_ptr<png_image>(new png_image(node_map_height, node_map_width));

			setXY(root, image, 0, 0);

			//loop through map and call parce
			build_lookup(root);

			std::string coords_string;
			for(int i = 0; i < m_coords_lookup->size(); i++){
				coords_string += "[";
				coords_string += std::to_string(m_coords_lookup->at(i).at(0)) + ",";
				coords_string += std::to_string(m_coords_lookup->at(i).at(1)) + ",";
				coords_string += std::to_string(m_coords_lookup->at(i).at(2)) + ",";
				coords_string += std::to_string(m_coords_lookup->at(i).at(3));
				coords_string += "],";
			}

			node_map_logger() << "INFO: Coords string is " << coords_string << std::endl;
			node_map_logger.log();
			image->m_tExt.emplace("coords_lookup", coords_string);

			std::string index_string;
			for(int i = 0; i < m_index_lookup->size(); i++){
				index_string += "[";
				index_string += std::to_string(m_index_lookup->at(i).at(0)) + ",";
				index_string += std::to_string(m_index_lookup->at(i).at(1)) + ",";
				index_string += std::to_string(m_index_lookup->at(i).at(2)) + ",";
				index_string += std::to_string(m_index_lookup->at(i).at(3)) + ",";
				index_string += std::to_string(m_index_lookup->at(i).at(4)) + ",";
				index_string += std::to_string(m_index_lookup->at(i).at(5));
				index_string += "],";
			}

			node_map_logger() << "INFO: Index string is " << index_string << std::endl;
			node_map_logger.log();
			image->m_tExt.emplace("index_lookup", index_string);

			std::string block_string;
			for(auto it = m_block_lookup->begin(); it != m_block_lookup->end(); it++){
				block_string += it->first;
				block_string += ":";
				block_string += std::to_string(it->second);
				block_string += ",";
			}

			node_map_logger() << "INFO: Block string is " << block_string << std::endl;
			node_map_logger.log();
			image->m_tExt.emplace("block_lookup", block_string);

			image->write("./resourses/texture_atlas.png");

			node_map_logger() << "INFO: Saved texture_atlas\n";
			node_map_logger.log();
		}else{
			node_map_logger(__FILE__,__LINE__) << "ERROR: trying to build texture atlas with no images.\n";
			node_map_logger.log();
		}
	}
};

#endif //TEXTURE_ATLAS_MAP_H