#include "voxel_scene.h"

#include <vector>

#include "gui_widgets.h"

voxel_scene::voxel_scene(client* Client):Client(Client){
    GUI_render = std::shared_ptr<gui_renderer>(new gui_renderer(Client->win, Client->ctx));

	m_octaves = 3;
	m_frequency = 0.00973;
	m_amplitude = 7;

    live = false;                 
}

void voxel_scene::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	if(Client->ctx->text_edit.active == nk_true){
		terminal->key_callback(window, key, scancode, action, mods);
	}else{
		if(key == GLFW_KEY_UP && action == GLFW_PRESS)
			cameraVector[2]++;
		else if(key == GLFW_KEY_DOWN && action == GLFW_PRESS)
			cameraVector[2]--;
		else if(key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
			cameraVector[0]++;
		else if(key == GLFW_KEY_LEFT && action == GLFW_PRESS)
			cameraVector[0]--;
	}
}
void voxel_scene::mouse_button_callback(GLFWwindow* window, int button, int action, int mods){}
void voxel_scene::scroll_callback(GLFWwindow* window, double xoffset, double yoffset){}
void voxel_scene::cursor_position_callback(GLFWwindow* window, double xpos, double ypos){}

void voxel_scene::create(){
	terminal = std::shared_ptr<gui_widgets::Terminal>(new gui_widgets::Terminal(Client->win, Client->ctx));

	SimpleVolume<uint8_t> volData(PolyVox::Region(Vector3DInt32(0,0,0), Vector3DInt32(63, 63, 63)));

	//buildFromHeightMap(volData);

	createSphereInVolume(volData, 30);

	SurfaceMesh<PositionMaterial> mesh;
	CubicSurfaceExtractor< SimpleVolume<uint8_t> > surfaceExtractor(&volData, volData.getEnclosingRegion(), &mesh);
	surfaceExtractor.execute();

	//Convienient access to the vertices and indices
	const std::vector<uint32_t>& vecIndices = mesh.getIndices();
	const std::vector<PositionMaterial>& vecVertices = mesh.getVertices();

	numVert = vecVertices.size();
	numInd = vecIndices.size();
	// debug mesh data
	/*printf("Indices\n");
	for(int i = 0; i < vecIndices.size(); i++){
		printf("%d\n", vecIndices[i]);
	}

	printf("vertices\n");
	for(int i = 0; i < vecVertices.size(); i++){
		Vector3DFloat pos = vecVertices[i].getPosition();
		printf("(%f, %f, %f) (%f)\n",pos.getX(),pos.getY(),pos.getZ(), vecVertices[i].getMaterial());
	}*/

	program = glCreateProgram();

	GLuint vs = load_shader_file("./shaders/warsim.voxel_scene.vs.glsl", GL_VERTEX_SHADER);
	GLuint gs = load_shader_file("./shaders/warsim.voxel_scene.gs.glsl", GL_GEOMETRY_SHADER);
    GLuint fs = load_shader_file("./shaders/warsim.voxel_scene.fs.glsl", GL_FRAGMENT_SHADER);

    glAttachShader(program, vs);
    glAttachShader(program, gs);
    glAttachShader(program, fs);

    glLinkProgram(program);

    // uniforms
    perspectiveLocation = glGetUniformLocation(program, "perspective");
    cameraLocation = glGetUniformLocation(program, "camera");

    glUseProgram(program);
    glUniformMatrix4fv(perspectiveLocation, 1, GL_TRUE, perspectiveMatrix);
    glUniform4fv(cameraLocation, 1, cameraVector);

    // we don't need the shader objects after linking
    glDetachShader(program, vs);
    glDetachShader(program, gs);
    glDetachShader(program, fs);

    glDeleteShader(vs);
    glDeleteShader(gs);
    glDeleteShader(fs);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

	//Build an OpenGL index buffer
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	pIndices = static_cast<const GLvoid*>(&(vecIndices[0]));		
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, vecIndices.size() * sizeof(uint32_t), pIndices, GL_STATIC_DRAW);

	//Build an OpenGL vertex buffer
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	const GLvoid* pVertices = static_cast<const GLvoid*>(&(vecVertices[0]));	
	glBufferData(GL_ARRAY_BUFFER, vecVertices.size() * sizeof(PositionMaterial), pVertices, GL_STATIC_DRAW);

	m_uBeginIndex = 0;
	m_uEndIndex = vecIndices.size();

    background = nk_rgb(28,48,62);

	live = true;
}

void voxel_scene::destroy(){
	//delete terminal;
	glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);
}

void voxel_scene::render(){
   if(!live)
        create();

    glfwGetWindowSize(Client->win, &window_width, &window_height);

    // populate gui command queue
    gui_widgets::game_menu(Client);
    draw_demo();
    terain_widget();
    terminal->render();

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    // get the program object
    glUseProgram(program);
    glBindVertexArray(vao);

    // Attribute Layouts
    static int modelspace_position = 0;

    // send modelspace coords
    glEnableVertexAttribArray(modelspace_position);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(modelspace_position, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	nk_color_fv(bg, background);

    // Set up window context
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(bg[0], bg[1], bg[2], bg[3]);

    // Change Aspect ratio to match window size
    perspectiveMatrix[0] = frustromScale / (window_width / (float)window_height);
    glUniformMatrix4fv(perspectiveLocation, 1, GL_TRUE, perspectiveMatrix);
    // move camera
    glUniform4fv(cameraLocation, 1, cameraVector);

	glDrawRangeElements(GL_TRIANGLES, m_uBeginIndex, m_uEndIndex-1, m_uEndIndex - m_uBeginIndex, GL_UNSIGNED_INT, 0);

    // Draw commands
    //glDrawElements(GL_TRIANGLES, numInd, GL_UNSIGNED_INT, pIndices);

    glDisableVertexAttribArray(modelspace_position);

    glUseProgram(0);

	GUI_render->gui_render();
}

void voxel_scene::buildFromHeightMap(SimpleVolume<uint8_t>& volData){
	for(int z = 0; z < volData.getDepth(); z++){
		for(int x = 0; x < volData.getWidth(); x++){
			//printf("y is %.10f\n", perlin_noise::get(x, 0, z));
			//printf("y is %f\n", perlin_noise::convert_range(perlin_noise::get(x, 0, z), std::numeric_limits<float>::max(), std::numeric_limits<float>::lowest(), volData.getHeight() - 1, 0));
			int y = (int)perlin_noise::convert_range(perlin_noise::get(x, 0, z, 3, m_octaves, m_frequency, m_amplitude), 1, 0, volData.getHeight() - 1, 0);
			printf("y is %d and height is %d\n", y, volData.getHeight());
			for(int i = 0; i <= y; i++)
				volData.setVoxelAt(x, i, z, 255);
		}
	}
	//exit(1);
}

void voxel_scene::createSphereInVolume(SimpleVolume<uint8_t>& volData, float fRadius){
	//This vector hold the position of the center of the volume
	Vector3DFloat v3dVolCenter(volData.getWidth() / 2, volData.getHeight() / 2, volData.getDepth() / 2);

	//This three-level for loop iterates over every voxel in the volume
	for (int z = 0; z < volData.getDepth(); z++){
		for (int y = 0; y < volData.getHeight(); y++){
			for (int x = 0; x < volData.getWidth(); x++){
				//Store our current position as a vector...
				Vector3DFloat v3dCurrentPos(x,y,z);	
				//And compute how far the current position is from the center of the volume
				float fDistToCenter = (v3dCurrentPos - v3dVolCenter).length();

				uint8_t uVoxelValue = 0;

				//If the current voxel is less than 'radius' units from the center then we make it solid.
				if(fDistToCenter <= fRadius){
					//Our new voxel value
					uVoxelValue = 255;
				}

				//Wrte the voxel value into the volume	
				volData.setVoxelAt(x, y, z, uVoxelValue);
			}
		}
	}
}

void voxel_scene::terain_widget(){
    if(nk_begin(Client->ctx, "World gen", nk_rect(50, 50, 230, 250), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE)){

        nk_layout_row_static(Client->ctx, 30, 80, 1);
        if(nk_button_label(Client->ctx, "Gen World")){
        	destroy();
        	create();
        }

        nk_layout_row_dynamic(Client->ctx, 25, 1);
        nk_property_float(Client->ctx, "Octaves:", 1, &m_octaves, 10, 1, 1);
        nk_property_float(Client->ctx, "Frequency:", 0, &m_frequency, 50, 0.0625, 1);
       	nk_property_float(Client->ctx, "Amplitude:", 0, &m_amplitude, 50, 0.0625, 1);

    }
    nk_end(Client->ctx);
}

void voxel_scene::draw_demo(){
    if(nk_begin(Client->ctx, "Visual Control", nk_rect(50, 50, 230, 250), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE)){
        
        enum{X, Y, Z};
        static int op = X;
        //static int property = 20;

        nk_layout_row_static(Client->ctx, 30, 80, 1);
        if(nk_button_label(Client->ctx, "Show Terminal")){
        	nk_window_show(Client->ctx, terminal->get_title(), NK_SHOWN);
        }

        nk_layout_row_dynamic(Client->ctx, 30, 2);
        if(nk_option_label(Client->ctx, "X", op == X)) op = X;
        if(nk_option_label(Client->ctx, "Y", op == Y)) op = Y;
        if(nk_option_label(Client->ctx, "Z", op == Z)) op = Z;

        nk_layout_row_dynamic(Client->ctx, 25, 1);
        if(op == X)
            nk_property_float(Client->ctx, "X position:", -500, &cameraVector[0], 500, 1, 1);
        else if(op == Y)
            nk_property_float(Client->ctx, "Y position:", -500, &cameraVector[1], 500, 1, 1);
        else
        	nk_property_float(Client->ctx, "Z position:", -500, &cameraVector[2], 500, 1, 1);

        {
            nk_layout_row_dynamic(Client->ctx, 20, 1);
            nk_label(Client->ctx, "background:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(Client->ctx, 25, 1);
            if (nk_combo_begin_color(Client->ctx, background, nk_vec2(nk_widget_width(Client->ctx),400))) {
                nk_layout_row_dynamic(Client->ctx, 120, 1);
                background = nk_color_picker(Client->ctx, background, NK_RGBA);
                nk_layout_row_dynamic(Client->ctx, 25, 1);
                background.r = (nk_byte)nk_propertyi(Client->ctx, "#R:", 0, background.r, 255, 1,1);
                background.g = (nk_byte)nk_propertyi(Client->ctx, "#G:", 0, background.g, 255, 1,1);
                background.b = (nk_byte)nk_propertyi(Client->ctx, "#B:", 0, background.b, 255, 1,1);
                background.a = (nk_byte)nk_propertyi(Client->ctx, "#A:", 0, background.a, 255, 1,1);
                nk_combo_end(Client->ctx);
            }
        }
    }
    nk_end(Client->ctx);
}