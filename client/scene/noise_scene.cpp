#include "noise_scene.h"

noise_scene::noise_scene(client* Client):Client(Client){
    GUI_render = std::shared_ptr<gui_renderer>(new gui_renderer(Client->win, Client->ctx));

    live = false;                                         
}

void noise_scene::create(){
    interations = 0;

    program = glCreateProgram();

    GLuint vs = load_shader_file("./shaders/warsim.noise_scene.vs.glsl", GL_VERTEX_SHADER);
    GLuint fs = load_shader_file("./shaders/warsim.noise_scene.fs.glsl", GL_FRAGMENT_SHADER);

    glAttachShader(program, vs);
    glAttachShader(program, fs);

    glLinkProgram(program);

    timeLocation = glGetUniformLocation(program, "time");

    //glUseProgram(program);
    //glUniform1i(timeLocation, time(NULL));

    // we don't need the shader objects after linking
    glDetachShader(program, vs);
    glDetachShader(program, fs);

    glDeleteShader(vs);
    glDeleteShader(fs);

	glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    background = nk_rgb(28,48,62);

    live = true;                                             
}

void noise_scene::destroy(){
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);
}

// As long as we use the right program the shaders are invoked
// by and draw commands
void noise_scene::render(){
    if(!live)
        create();

    glfwGetWindowSize(Client->win, &window_width, &window_height);

    // populate gui command queue
    gui_widgets::game_menu(Client);

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    // get the program object
    glUseProgram(program);
    glBindVertexArray(vao);

    // Attribute Layouts
    static int modelspace_position = 0;

    // send modelspace coords
    glEnableVertexAttribArray(modelspace_position);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(modelspace_position, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glUniform1i(timeLocation, interations++);

    // convert nuklear color to vec4
    nk_color_fv(bg, background);

    // Set up window context
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(bg[0], bg[1], bg[2], bg[3]);

    // Draw commands
    glDrawArrays(GL_TRIANGLES, 0, 6);
    //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, index);

    glDisableVertexAttribArray(0);

    GUI_render->gui_render();
}