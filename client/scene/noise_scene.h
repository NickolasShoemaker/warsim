#ifndef NOISE_SCENE_H
#define NOISE_SCENE_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>

#include "global_scene_map.h"
#include "nk_wrapper.h"
#include "gui_renderer.h"

#include "shaders_api.h"
#include "gui_widgets.h"

#include "client.h"

/*
* We want to draw a rectangle the size of the screen
* then use a custom fragment shader to color the pixels
* based on a noise value
*/
class noise_scene : public scene{
    client* Client;
	std::shared_ptr<gui_renderer> GUI_render;

	int interations;

	GLuint program;
	GLuint vao;
	GLuint vertexbuffer;

	GLuint timeLocation;

	const GLfloat width = 1.0f;
    const GLfloat length = 1.0f;
    const GLfloat height = 1.0f;
    const GLfloat scale = 1.0f;

	const GLfloat g_vertex_buffer_data[32] = {
        -length, height, -width, scale,  // top front left, 0
       length,  height, -width, scale,   // top front right, 1
    	-length, -height, -width, scale,  // bottom front left, 6

        -length, -height, -width, scale,  // bottom front left, 6
       length, -height, -width, scale,   // bottom front right, 7
       length,  height, -width, scale,   // top front right, 1
    };

	int window_width, window_height;
	struct nk_color background;
	float bg[4];
public:
	noise_scene(client* Client);
	~noise_scene(){
	    //delete GUI_render;
	    if(live)
	        destroy();
	}

	void create();
	void destroy();

	void render();

    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){}
    void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){}
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){}
    void cursor_position_callback(GLFWwindow* window, double xpos, double ypos){}
};

#endif //NOISE_SCENE_H