#ifndef RENDER_SCENE_H
#define RENDER_SCENE_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>

#include "global_scene_map.h"
#include "nk_wrapper.h"
#include "gui_renderer.h"
#include "png_image.h"

#include "gui_widgets.h"

#include "client.h"

class render_scene : public scene{
  client* Client;
	std::shared_ptr<gui_renderer> GUI_render;

	GLuint program;
	GLuint vao;
	GLuint vertexbuffer;

	GLuint texture_sampler;

	GLuint texture;
	long unsigned int texture_w;
	long unsigned int texture_h;

	GLint perspectiveLocation;
	GLint cameraLocation;
	GLint SamplerLocation;

	float frustromScale = 1.0f;
	float fzNear = 0.1f;
	float fzFar = 10.0f;

	const GLfloat width = 1.0f;
    const GLfloat length = 1.0f;
    const GLfloat height = 1.0f;
    const GLfloat scale = 2.0f;

	float perspectiveMatrix[16] = {
	    frustromScale, 0.0f,          0.0f,                                0.0f,
	    0.0f,          frustromScale, 0.0f,                                0.0f,
	    0.0f,          0.0f,          (fzFar + fzNear) / (fzNear - fzFar), (2 * fzFar * fzNear) / (fzNear - fzFar),
	    0.0f,          0.0f,          -1.0f,                               0.0f,
	};

	float cameraVector[4] = {
	    4.0f, 1.0f, -7.0f, 0.0f,
	};

	// glDrawElements information, doesn't work with texturing
/*	const unsigned char index[36] = {
	    0, 1, 2,        2, 1, 3,  // top
	    6, 7, 4,        4, 7, 5,  // bottom
	    4, 5, 3,        4, 2, 3,  // back
	    6, 7, 1,        6, 0, 1,  // front
	    0, 2, 4,        4, 2, 6,  // left
	    1, 3, 5,        5, 3, 7,  // right
	};

	const GLfloat g_vertex_buffer_data[32] = {
       // top
        -length, height, -width, scale,  // top front left, 0
       length,  height, -width, scale,   // top front right, 1
       -length, height, width, scale,  // top back left, 2
       length,  height, width, scale,   // top back right, 3
       // bottom
       -length, -height, width, scale,  // bottom back left, 4
       length, -height, width, scale,   // bottom back right, 5
        -length, -height, -width, scale,  // bottom front left, 6
       length, -height, -width, scale,   // bottom front right, 7
/*
    };*/

       // width goes into the screen
       // length goes across the screen
       // height is up and down

	const GLfloat g_vertex_buffer_data[216] = {
       // top
       -length, height,  width, scale, 		0.0f, 0.0f,  // top back left, 2
        length, height, -width, scale,   	1.0f, 1.0f,  // top front right, 1
       -length, height, -width, scale,		0.0f, 1.0f,  // top front left, 0

        length, height,  width, scale,   	1.0f, 0.0f,  // top back right, 3
        length, height, -width, scale,   	1.0f, 1.0f,  // top front right, 1
       -length, height,  width, scale, 		0.0f, 0.0f,  // top back left, 2

       // bottom
        -length, -height, -width, scale,  	0.0f, 0.0f, // bottom front left, 6
       length, -height, -width, scale,    	1.0f, 0.0f, // bottom front right, 7
       -length, -height, width, scale,  	0.0f, 1.0f, // bottom back left, 4
       
		-length, -height, width, scale,  	0.0f, 1.0f, // bottom back left, 4
		length, -height, -width, scale,   	1.0f, 0.0f, // bottom front right, 7
       length, -height, width, scale,    	1.0f, 1.0f, // bottom back right, 5

       // front
       -length, -height, width, scale,  	0.0f, 0.0f, // bottom back left, 4
		length, -height, width, scale,  	1.0f, 0.0f, // bottom back right, 5
		length,  height, width, scale,  	1.0f, 1.0f, // top back right, 3

       -length,  height, width, scale, 		0.0f, 1.0f, // top back left, 2
	   -length, -height, width, scale,  	0.0f, 0.0f, // bottom back left, 4
        length,  height, width, scale,   	1.0f, 1.0f, // top back right, 3

       // back
        length,  height, -width, scale,   	1.0f, 1.0f, // top front right, 1
        length, -height, -width, scale,   	0.0f, 1.0f, // bottom front right, 7
       -length, -height, -width, scale,  	1.0f, 0.0f, // bottom front left, 6

        -length,  height, -width, scale,  	0.0f, 0.0f, // top front left, 0
         length,  height, -width, scale,   	1.0f, 1.0f, // top front right, 1
		-length, -height, -width, scale, 	1.0f, 0.0f, // bottom front left, 6

       // left
	   -length, -height, -width, scale,		0.0f, 0.0f, // bottom front left, 6
       -length, height, width, scale,    	1.0f, 1.0f, // top back left, 2
       -length, height, -width, scale,  	0.0f, 1.0f, // top front left, 0

       -length, height, width, scale,  		1.0f, 1.0f, // top back left, 2
       -length, -height, -width, scale,		0.0f, 0.0f, // bottom front left, 6
       -length, -height, width, scale, 		1.0f, 0.0f, // bottom back left, 4

       // right
       length,  height, -width, scale,   	1.0f, 1.0f, // top front right, 1
       length,  height, width, scale,    	0.0f, 1.0f, // top back right, 3
       length, -height, -width, scale,  	1.0f, 0.0f,  // bottom front right, 7 

       length, -height, width, scale,   	0.0f, 0.0f, // bottom back right, 5
       length, -height, -width, scale,  	1.0f, 0.0f,  // bottom front right, 7
	   length,  height, width, scale,  		0.0f, 1.0f, // top back right, 3
    };

	int window_width, window_height;
	struct nk_color background;
	float bg[4];

	void draw_demo();
public:
	render_scene(client* Client);
	~render_scene(){
	    //delete GUI_render;
	    if(live)
	        destroy();
	}

	// create or destroy scene
	void create();
	void destroy();

	//bool live;

	void render();

  void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){}
  void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){}
  void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){}
  void cursor_position_callback(GLFWwindow* window, double xpos, double ypos){}
};

#endif //RENDER_SCENE_H