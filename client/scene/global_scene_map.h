#ifndef GLOBAL_SCENE_MAP
#define GLOBAL_SCENE_MAP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

/*
* A collection of drawn output and accepted input
* and the logic to go with it
*
* Abstract base class for any scene
*/
class scene{
public:
	// These are more of an internal factory without internal data
	// until create or destroy are used
	scene(){}

	// create or destroy scene
	virtual void create() = 0;
	virtual void destroy() = 0;

	// is the scene ready to render or is it a husk
	bool live;

	virtual void render() = 0;

	virtual void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) = 0;
	virtual void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) = 0;
	virtual void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) = 0;
	virtual void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) = 0;
};

#endif //GLOBAL_SCENE_MAP