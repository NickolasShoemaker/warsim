#include "render_scene.h"

render_scene::render_scene(client* Client):Client(Client){
    GUI_render = std::shared_ptr<gui_renderer>(new gui_renderer(Client->win, Client->ctx));  

  live = false;                                          
}

void render_scene::create(){
    program = glCreateProgram();

    GLuint vs = load_shader_file("./shaders/warsim.render_scene.vs.glsl", GL_VERTEX_SHADER);
    GLuint fs = load_shader_file("./shaders/warsim.render_scene.fs.glsl", GL_FRAGMENT_SHADER);

    glAttachShader(program, vs);
    glAttachShader(program, fs);

    glLinkProgram(program);

    // uniforms
    perspectiveLocation = glGetUniformLocation(program, "perspective");
    cameraLocation = glGetUniformLocation(program, "camera");
    SamplerLocation = glGetUniformLocation(program, "text_sampler");

    glUseProgram(program);
    glUniformMatrix4fv(perspectiveLocation, 1, GL_TRUE, perspectiveMatrix);
    glUniform4fv(cameraLocation, 1, cameraVector);

    // we don't need the shader objects after linking
    glDetachShader(program, vs);
    glDetachShader(program, fs);

    glDeleteShader(vs);
    glDeleteShader(fs);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    //GLint format;
    png_image text_image("./resourses/grass.side.png");
    texture = text_image.GLtexture();

    glGenSamplers(1, &texture_sampler);
    glSamplerParameteri(texture_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(texture_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glSamplerParameteri(texture_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glSamplerParameteri(texture_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    background = nk_rgb(28,48,62);

    live = true;
}

void render_scene::destroy(){
    glDeleteTextures(1, &texture);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(program);
}

// As long as we use the right program the shaders are invoked
// by and draw commands
void render_scene::render(){
    // If we haven't set up yet do so
    if(!live)
        create();

    glfwGetWindowSize(Client->win, &window_width, &window_height);

    // populate gui command queue
    gui_widgets::game_menu(Client);
    draw_demo();

    //glEnable(GL_BLEND);
    //glBlendEquation(GL_FUNC_ADD);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glActiveTexture(GL_TEXTURE0);

    // get the program object
    glUseProgram(program);
    glBindVertexArray(vao);

    // Attribute Layouts
    static int modelspace_position = 0;
    static int texture_coords = 1;

    // send modelspace coords
    glEnableVertexAttribArray(modelspace_position);
    glEnableVertexAttribArray(texture_coords);

    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(modelspace_position, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void*)0);
    glVertexAttribPointer(texture_coords, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void*)(sizeof(float) * 4));

    // convert nuklear color to vec4
    nk_color_fv(bg, background);

    // Set up window context
    glViewport(0, 0, window_width, window_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(bg[0], bg[1], bg[2], bg[3]);

    // Change Aspect ratio to match window size
    perspectiveMatrix[0] = frustromScale / (window_width / (float)window_height);
    glUniformMatrix4fv(perspectiveLocation, 1, GL_TRUE, perspectiveMatrix);
    // move camera
    glUniform4fv(cameraLocation, 1, cameraVector);

    glBindTexture(GL_TEXTURE_2D, texture);
    //glUniform1i(SamplerLocation, texture);
    glBindSampler(SamplerLocation, texture_sampler);

    // Draw commands
    glDrawArrays(GL_TRIANGLES, 0, 144);
    //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, index);

    glDisableVertexAttribArray(texture_coords);
    glDisableVertexAttribArray(modelspace_position);

    glUseProgram(0);

    GUI_render->gui_render();
}

//---------------------------
// PRIVATE
//---------------------------

/*
* Controls the color of the background
*/
void render_scene::draw_demo(){
    //struct nk_panel layout;
    if(nk_begin(Client->ctx, "Visual Control", nk_rect(50, 50, 230, 250), NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE)){
        
        enum{X, Y};
        static int op = X;
        //static int property = 20;

        nk_layout_row_static(Client->ctx, 30, 80, 1);
        if(nk_button_label(Client->ctx, "button"))
            fprintf(stdout, "button pressed\n");

        nk_layout_row_dynamic(Client->ctx, 30, 2);
        if(nk_option_label(Client->ctx, "X", op == X)) op = X;
        if(nk_option_label(Client->ctx, "Y", op == Y)) op = Y;

        nk_layout_row_dynamic(Client->ctx, 25, 1);
        if(op == X)
            nk_property_float(Client->ctx, "X position:", -100, &cameraVector[0], 100, 1, 1);
        else
            nk_property_float(Client->ctx, "Y position:", -100, &cameraVector[1], 100, 1, 1);

        {
            //struct nk_panel combo;
            nk_layout_row_dynamic(Client->ctx, 20, 1);
            nk_label(Client->ctx, "background:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(Client->ctx, 25, 1);
            if (nk_combo_begin_color(Client->ctx, background, nk_vec2(nk_widget_width(Client->ctx),400))) {
                nk_layout_row_dynamic(Client->ctx, 120, 1);
                background = nk_color_picker(Client->ctx, background, NK_RGBA);
                nk_layout_row_dynamic(Client->ctx, 25, 1);
                background.r = (nk_byte)nk_propertyi(Client->ctx, "#R:", 0, background.r, 255, 1,1);
                background.g = (nk_byte)nk_propertyi(Client->ctx, "#G:", 0, background.g, 255, 1,1);
                background.b = (nk_byte)nk_propertyi(Client->ctx, "#B:", 0, background.b, 255, 1,1);
                background.a = (nk_byte)nk_propertyi(Client->ctx, "#A:", 0, background.a, 255, 1,1);
                nk_combo_end(Client->ctx);
            }
        }
    }
    nk_end(Client->ctx);
}
