#ifndef VOXEL_SCENE_H
#define VOXEL_SCENE_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>

#include <limits>

#include "global_scene_map.h"
#include "nk_wrapper.h"
#include "gui_renderer.h"

#include "Terminal.h"

#include "CubicSurfaceExtractor.h"
#include "SurfaceMesh.h"
#include "SimpleVolume.h"

#include "perlin_noise.h"

#include "client.h"

using namespace PolyVox;

class voxel_scene : public scene{
	client* Client;
	std::shared_ptr<gui_renderer> GUI_render;

	std::shared_ptr<gui_widgets::Terminal> terminal;

	GLuint program;
	GLuint vao;
	GLuint indexBuffer;
	GLuint vertexBuffer;

	GLuint m_uBeginIndex;
	GLuint m_uEndIndex;
	GLuint noOfIndices;

	int window_width, window_height;
	struct nk_color background;
	float bg[4];

	GLint perspectiveLocation;
	GLint cameraLocation;

	float frustromScale = 1.0f;
	float fzNear = 0.1f;
	float fzFar = 100.0f;

	float perspectiveMatrix[16] = {
	    frustromScale, 0.0f,          0.0f,                                0.0f,
	    0.0f,          frustromScale, 0.0f,                                0.0f,
	    0.0f,          0.0f,          (fzFar + fzNear) / (fzNear - fzFar), (2 * fzFar * fzNear) / (fzNear - fzFar),
	    0.0f,          0.0f,          -1.0f,                               0.0f,
	};

	float cameraVector[4] = {
	    -32.0f, -32.0f, -80.0f, 0.0f,
	};

	const GLvoid* pIndices;
	int numVert;
	int numInd;

	float m_octaves;
	float m_frequency;
	float m_amplitude;

	void createSphereInVolume(SimpleVolume<uint8_t>& volData, float fRadius);
	void buildFromHeightMap(SimpleVolume<uint8_t>& volData);
	void draw_demo();
	void terain_widget();
public:
	voxel_scene(client* Client);
	~voxel_scene(){
	    //delete GUI_render;
	    if(live)
	        destroy();
	}

	// create or destroy scene
	void create();
	void destroy();

	void render();

	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
	void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
};

#endif //VOXEL_SCENE_H