#include "shaders_api.h"

GLuint load_shader(const char* const* code, GLenum shader_type){
	// Create shader
	GLuint shader_id = glCreateShader(shader_type);
	glShaderSource(shader_id, 1, code, NULL);
	glCompileShader(shader_id);

	// Error check
 	GLint compile_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compile_status);
    if(compile_status != GL_TRUE){
    	print_shader_log(shader_id);
    	glDeleteShader(shader_id);
    	exit(1);
    }

    return shader_id;
}

GLuint load_shader_file(const char* cfile, GLenum shader_type){

	FILE* fileptr;
	size_t filesize;
	char* code;

	fileptr = fopen(cfile, "rb");

	if(!fileptr){
		fprintf(stderr, "Error: could not load shader at location %s no such location found.\n", cfile);
		exit(1);
	}

	fseek(fileptr, 0, SEEK_END);
	filesize = ftell(fileptr);
	fseek(fileptr, 0, SEEK_SET);

	code = new char[filesize + 1];

	if(!code){
		fprintf(stderr, "Error: could not load shader at location %s could not read file.\n", cfile);
		exit(1);
	}

	fread(code, 1, filesize, fileptr);
	code[filesize] = 0;
	fclose(fileptr);

	GLuint shader_id = load_shader(&code, shader_type);
	delete[] code;

	return shader_id;
}

void print_program_log(GLuint program){
    GLint len;

    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
    char* buffer = new char[len + 1];
    glGetProgramInfoLog(program, len, NULL, buffer);
    buffer[len] = 0;	//TODO: check if we need to do this, it may already be null terminated
    printf("%s\n", buffer);
    delete[] buffer;
}

void print_shader_log(GLuint shader){
    GLint len;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    char* buffer = new char[len + 1];
    glGetShaderInfoLog(shader, len, NULL, buffer);
    buffer[len] = 0;	//TODO: check if we need to do this, it may already be null terminated
    printf("%s\n", buffer);
    delete[] buffer;
}
