/******************************************************************************
* War sim - This is an experiment in OpenGL 3.0+ and nuklear rendering        *
*                                                                             *
* Nick Shoemaker                                                              *
*                                                                             *
*                                                                             *
*                                                                             *
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "global_scene_map.h"

#include "render_scene.h"
#include "noise_scene.h"
#include "voxel_scene.h"

#include "client.h"

#include "logger.h"

/*
* Client thread is the main thread
*
* We want server to be run as a thread or a dedicated process
*/
int main(void){
    logger main_log("./log/main.log");

    main_log() << "Warsim Starting" << std::endl << "-----------------------------------------------------------" << std::endl;
    main_log.log();

    client Client;
    //server Server
    //std::thread server_thread(Server);

    // set up the scene map
    Client.register_scene("Noise Screen", std::shared_ptr<scene>(new noise_scene(&Client)));
    Client.register_scene("Game Screen", std::shared_ptr<scene>(new render_scene(&Client)));
    Client.register_scene("Voxel Screen", std::shared_ptr<scene>(new voxel_scene(&Client)));
    Client.set_current_scene("Voxel Screen");

    Client();

    //send signal to kill server
    //server_thread.join()

    main_log() << "-----------------------------------------------------------" << std::endl;
    main_log.log();

    return 0;
}

