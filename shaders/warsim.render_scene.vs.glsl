#version 330 core 

layout(location = 0) in vec4 vertexPosition_modelspace;
layout(location = 1) in vec2 text_coord;

uniform mat4 perspective;
uniform vec4 camera;

noperspective out vec2 textcoord;

void main(void){ 
	textcoord = text_coord;

	vec4 place = vertexPosition_modelspace + camera; 
	gl_Position = perspective * place;
}