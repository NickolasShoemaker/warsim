#version 330 core
                              
layout(location = 0) in vec4 vertexPosition_modelspace;
uniform int time;

void main(void){                                              
    gl_Position = vertexPosition_modelspace;   
}                                              