#version 330 core 

layout(location = 0) in vec3 vertexPosition_modelspace;
//layout(location = 1) in int blockID;

//out int vertexBlockID;

uniform mat4 perspective;
uniform vec4 camera;

void main(void){ 
	vec4 place = vec4(vertexPosition_modelspace, 1.0) + camera; 
	gl_Position = perspective * place;
}