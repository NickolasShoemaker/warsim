#version 330 core

in GS_OUT{
	vec3 color;
} fs_in;

out vec3 color;

void main(void){
	color = fs_in.color;
	//color = vec3(255, 0, 0);
}