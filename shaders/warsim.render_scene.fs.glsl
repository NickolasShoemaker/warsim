#version 330 core

uniform sampler2D text_sampler;

out vec3 color;

noperspective in vec2 textcoord;

void main(void){
	color = texture(text_sampler, textcoord).rgb;
}