#version 330

layout (triangles) in;
layout (triangle_strip) out;
layout (max_vertices = 3) out;

out GS_OUT{
	vec3 color;
} gs_out;

// determin the normal so we know where the user is looking
// determin where the face is and set texture coords

/*
* Set the data for this triangle
*/
void set_data(in vec3 color){
	gs_out.color = color;
	gl_Position = gl_in[0].gl_Position;
	EmitVertex();
	gs_out.color = color;
	gl_Position = gl_in[1].gl_Position;
	EmitVertex();
	gs_out.color = color;
	gl_Position = gl_in[2].gl_Position;
	EmitVertex();
}

void main(void){

	/*
	* which of the 6 cardinal directions are we in
	*
	* Calculate two vectors in the plane of the input triangle
	*/
	vec3 ab = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 ac = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 normal = normalize(cross(ab, ac));

	/*
	* Which way is the triangle facing
	*/
	vec3 direction = ab + ac;
	if(dot(ab, ac) != 0){
		vec3 bc = gl_in[2].gl_Position.xyz - gl_in[1].gl_Position.xyz;
		if(dot(ab, bc) == 0){
			direction = ab + bc;
		}else{
			direction = ac + bc;
		}
	}

	/*
	* Left (-x) plane
	*/
	if(normal.x > 0){
		if(direction.y < 0){
			if(direction.z < 0){ // (-z,-y)
				set_data(vec3(255, 0, 0));
			}else{				 // (z,-y)
				set_data(vec3(255, 0, 0));
			}
		}else{
			if(direction.z < 0){ // (-z,y)
				set_data(vec3(0, 0, 0));		// this
			}else{				 // (z,y)
				set_data(vec3(0, 255, 0));
			}
		}

	/*
	* Right (x)
	*/
	}else if(normal.x < 0){
		if(direction.y < 0){
			if(direction.z < 0){ // (-z,-y)
				set_data(vec3(255, 255, 0));		// this
			}else{				 // (z,-y)
				set_data(vec3(0, 0, 0));
			}
		}else{
			if(direction.z < 0){ // (-z,y)
				set_data(vec3(0, 255, 255));
			}else{				 // (z,y)
				set_data(vec3(0, 255, 0));
			}
		}

	/*
	* (-y, y) plane
	*/
	}else if(normal.y > 0){
		if(direction.z < 0){
			if(direction.x < 0){ // (-x,-z)
				set_data(vec3(255, 0, 0)); //set_data(vec3(5, 2, 91));
			}else{				 // (x,-z)
				set_data(vec3(0, 255, 255)); //set_data(vec3(18, 10, 252));
			}
		}else{
			if(direction.x < 0){ // (-x,z)
				set_data(vec3(255, 0, 0)); //set_data(vec3(86, 84, 153));
			}else{				 // (x,z)
				set_data(vec3(0, 255, 255)); //set_data(vec3(121, 118, 219));
			}
		}

	}else if(normal.y < 0){
		if(direction.z < 0){
			if(direction.x < 0){ // (-x,-z)
				set_data(vec3(0, 0, 255)); //set_data(vec3(219, 118, 205));
			}else{				 // (x,-z)
				set_data(vec3(255, 0, 255)); //set_data(vec3(102, 6, 89));
			}
		}else{
			if(direction.x < 0){ // (-x,z)
				set_data(vec3(0, 0, 255)); //set_data(vec3(79, 45, 74));
			}else{				 // (z,x)
				set_data(vec3(0, 255, 0)); //set_data(vec3(214, 113, 200));
			}
		}

	/*
	* (z) plane
	*/
	}else if(normal.z > 0){
		if(direction.y < 0){
			if(direction.x < 0){ // (-x,-y)
				set_data(vec3(255, 0, 0));
			}else{				 // (x,-y)
				set_data(vec3(0, 0, 0));
			}
		}else{
			if(direction.x < 0){ // (-x,y)
				set_data(vec3(0, 255, 0));
			}else{				 // (x,y)
				set_data(vec3(255, 0, 255));
			}
		}

	}else if(normal.z < 0){
		if(direction.y < 0){
			if(direction.x < 0){ // (-x,-y)
				set_data(vec3(0, 255, 255));
			}else{				 // (x,-y)
				set_data(vec3(255, 255, 255));
			}
		}else{
			if(direction.x < 0){ // (-x,y)
				set_data(vec3(255, 255, 0));
			}else{				 // (x,y)
				set_data(vec3(0, 0, 0));
			}
		}

	/*
	* If nothing else
	*/
	}else{
		set_data(vec3(0, 0, 0));
	}

	EndPrimitive();
}