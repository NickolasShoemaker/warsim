##########################################################
# Nick Shoemaker                                         #
# Use shell find to look through subdirs to find source  #
# and include files and compile. This allows changing    #
# project structure without worry about breaking make    #
##########################################################

# Install
BIN = warsim

# Flags
LIBS = -lglfw -lGL -lm -lGLU -lGLEW -lpng -lpthread -lboost_system -lboost_filesystem
CFLAGS = -std=c++11
CC = g++
INC = $(addprefix -I,$(shell find . -name "*.h" -printf "%h\n" | sort -u))

# Source files
C_FILES = $(shell find . -name "*.c")
CPP_FILES = $(shell find . -name "*.cpp")

SRC_DIRS = $(addprefix $(BDIR)/, $(shell find -name "*.cpp" -printf "%h\n" | sort -u))

# Object files
C_OBJ = $(addprefix $(BDIR)/,$(C_FILES:.c=.o))
CPP_OBJ = $(addprefix $(BDIR)/,$(CPP_FILES:.cpp=.o))

# Object depends for build
OBJ = $(C_OBJ) $(CPP_OBJ)

# The build directory
BDIR = bin

# Build program from objects
$(BIN): make_dirs $(OBJ)
	$(CC) $(OBJ) -o bin/$(BIN) $(CFLAGS) $(LIBS)

make_dirs:
	@mkdir -p bin
	@mkdir -p $(SRC_DIRS)

$(CPP_OBJ): $(BDIR)/%.o: %.cpp
	$(CC) $(INC) -c -g $(CFLAGS) $^ -o $@ 

$(C_OBJ): $(BDIR)/%.o: %.c
	$(CC) $(INC) -c -g $(CFLAGS) $^ -o $@

# clean all but nk_wrapper as it will not change and takes a while to compile
clean:
	rm -f bin/$(BIN) $(filter-out bin/./client/gui/nk_wrapper.o, $(OBJ))

cleanall:
	rm -f bin/$(BIN) $(OBJ)

# Shortcut to running
run:
	./$(BDIR)/$(BIN)

debug:
	gdb ./$(BDIR)/$(BIN)